package com.bit4id.identity;

//import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.service.BibliotecaMasterService;
import pe.gob.bnp.tramitedoc.util.FileOpenKM;
import pe.gob.bnp.tramitedoc.util.OpenKMDocumento;
import pe.gob.bnp.tramitedoc.util.ResultOpenKMDocumento;
import pe.gob.bnp.tramitedoc.util.VO;

@WebServlet("/Signing")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, maxFileSize = 1024 * 1024 * 15, maxRequestSize = 1024 * 1024
		* 30)
public class Signing extends HttpServlet {

	@Autowired
	BibliotecaMasterService bibliotecaMasterService;

	public static final String CARPETA_RAIZ = "/E_GESDOC";
	public static final String OPERACION_EXITO = "00000";

	private static final long serialVersionUID = 1L;

	public Signing() {
		super();

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("SIGNATURE MANAGEMENT SERVLET");

		ServletContext servletContext = getServletContext();
		String contentdir = servletContext.getRealPath(File.separator);
		String pathServlet = request.getServletPath();
		String fullPathServlet = request.getRequestURL().toString();
		int resInt = fullPathServlet.length() - pathServlet.length();

		String result = fullPathServlet.substring(0, resInt + 1);

		out.println("Context path: " + contentdir);
		out.println("Full Servlet path: " + fullPathServlet);
		out.println("Servlet path: " + pathServlet);
		out.println("Server path: " + result);

		out.close();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		BibliotecaMaster bibliotecaSeleccionada = new BibliotecaMaster();

		BibliotecaMasterService bibliotecaMasterService = (BibliotecaMasterService) WebApplicationContextUtils
				.getRequiredWebApplicationContext(getServletContext()).getBean("bibliotecaMasterService");

		PrintWriter out = response.getWriter();
		ServletContext servletContext = getServletContext();
		// String contentdir = servletContext.getRealPath(File.separator) + "//signed";

		byte[] pdfBytes = null;

		System.out.print(out);
		String pathServlet = request.getServletPath();
		String fullPathServlet = request.getRequestURL().toString();
		int resInt = fullPathServlet.length() - pathServlet.length();

		String signstore = fullPathServlet.substring(0, resInt + 1) + "signed";

		System.out.println(signstore);

		String fileName = null;
		String tempFileDestination = null;
		for (Part part : request.getParts()) {
			if (part.getName().equals("documentID")) {
				InputStream inputStream = part.getInputStream();

				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

				fileName = bufferedReader.readLine();
			} else if (part.getName().equals("attach")) {
				// tempFileDestination = contentdir + File.separator + "temp.pdf";
				// part.write(tempFileDestination);
				pdfBytes = new byte[part.getInputStream().available()];

				part.getInputStream().read(pdfBytes);

			}
		}
		long idBibliotecaMaster = Long.parseLong(fileName.replaceAll(".pdf", ""));

		bibliotecaSeleccionada = bibliotecaMasterService.consultarBibliotecaMaster(idBibliotecaMaster);

		ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
		OpenKMDocumento openKMDocumento = new OpenKMDocumento();
		FileOpenKM fileOpenKM = new FileOpenKM();

		Date fechaModificacion = new Date(); 

		fileOpenKM.setFileName(fileName);
		fileOpenKM.setFechaDocumento(new Date());
//		fileOpenKM.setFechaUpload("12/12/2012");
		fileOpenKM.setRawFile(pdfBytes);
		fileOpenKM.setExtension("pdf");

		resultOpenKMDocumento = openKMDocumento.registrarContenido(fileOpenKM);

		if (!VO.isNull(resultOpenKMDocumento)) {
			if (resultOpenKMDocumento.getCodigo().equals(OPERACION_EXITO)) {
				bibliotecaSeleccionada.setColumn1("1");
				bibliotecaSeleccionada.setColumn2(resultOpenKMDocumento.getUiid());
				// bibliotecaSeleccionada.setFechaModificacion(fechaModificacion);
				bibliotecaMasterService.actualizar(bibliotecaSeleccionada);
			}
		}

		// new File(tempFileDestination).renameTo(new File(contentdir + File.separator,
		// fileName));
		// response.sendRedirect("success.jsp?link=" + signstore + "/" + fileName);
		response.sendRedirect("http://rnb.bnp.gob.pe/#/verFirmaDocumento/" + bibliotecaSeleccionada.getNumeroRnb());
		// response.sendRedirect("http://sinabi.bnp.gob.pe:8080/sinabi/#/verFirmaDocumento/"+bibliotecaSeleccionada.getNumeroRnb());

		// response.sendRedirect("success.jsp");

	}

}
