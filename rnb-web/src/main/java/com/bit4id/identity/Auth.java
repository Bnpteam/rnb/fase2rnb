package com.bit4id.identity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Auth")
public class Auth extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Auth() {
        super();
     }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		//String htmlout="<br>GET EXECUTED";
		out.println("AUTH MANAGEMENT SERVLET");
		//response.sendRedirect("success.jsp?outprint=" + htmlout);
		out.close();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Enumeration<String> parameterNames = request.getParameterNames();
		String htmlout="";
		
		response.setContentType("text/html");
				
        while (parameterNames.hasMoreElements()) {
           String paramName = parameterNames.nextElement();
           if ("result".equals(paramName))
           {
        	   String prmRes = request.getParameter(paramName);
        	   String result = prmRes.substring(0, 2);
        	   htmlout = "<p></p>AUTHENTICATION: " + result + "<p></p>";
        	   
        	   // INSERT SERVER CODE HERE TO MANAGE THE AUTHENTICATION RESULTS
        	   // USING THE VARIABLE result:
        	   // ok -> AUTHENTICATION SUCCESSFULL
        	   // ko -> AUTHENTICATION FAILED
        	   
           }
           htmlout= htmlout + "<br>" + paramName;
           String[] paramValues = request.getParameterValues(paramName); 
           for (int i = 0; i < paramValues.length; i++) { 
                String paramValue = paramValues[i];
                htmlout= htmlout + "(" + paramValue + ")"; 
            } 
        }
        
        response.sendRedirect("success.jsp?outprint=" + htmlout);
       }

}
