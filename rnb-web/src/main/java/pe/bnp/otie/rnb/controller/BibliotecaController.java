package pe.bnp.otie.rnb.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.Biblioteca;
import pe.bnp.otie.rnb.service.BibliotecaService;

@RestController
@RequestMapping("/biblioteca")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class BibliotecaController {

	@Autowired
	BibliotecaService bibliotecaService;

	@RequestMapping(value = "/datosBiblioteca", method = RequestMethod.GET)
	public Biblioteca getConstanciaBibliotecaMaster(@RequestParam(required = true) Long codigoBibliotecaOrigen) {

		return bibliotecaService.buscarBibliotecaOrigen(codigoBibliotecaOrigen);
	}

	@RequestMapping(value = "/actualizarRegistrador", method = RequestMethod.POST)
	public Biblioteca getConstanciaBibliotecaMaster(@RequestBody Biblioteca biblioteca) {
		return bibliotecaService.actualizarRegistrador(biblioteca.getIdBiblioteca(), biblioteca.getIdRegistrador());
	}

	@RequestMapping(value = "/cantidadRegistros", method = RequestMethod.GET)
	public Long getCantidadRegistros() {

		return bibliotecaService.cantidadSolicitudBiblioteca();
	}

	@RequestMapping(value = "/bibliotecaByDate", method = RequestMethod.GET)
	public List<Biblioteca> getBibliotecaByDate(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date(); 

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bibliotecaService.listarBibliotecaByDate(start1, end1);
	}
	
	@RequestMapping(value = "/contarBibliotecaByDate", method = RequestMethod.GET)
	public Long contarBibliotecaByDate(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date(); 

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bibliotecaService.contarBibliotecaByDate(start1, end1);
	}
	
	
	@RequestMapping(value = "/buscarBiblioteca", method = RequestMethod.GET)
	public Biblioteca getBiblioteca(@RequestParam(required = true) Long idBiblioteca) {
		return bibliotecaService.buscarBiblioteca(idBiblioteca);
	}
	 

}
