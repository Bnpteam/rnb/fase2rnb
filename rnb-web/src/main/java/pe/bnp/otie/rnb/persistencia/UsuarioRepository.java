package pe.bnp.otie.rnb.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.bnp.otie.rnb.dominio.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findByIdUsuario(Long idUsuario);
		
}
