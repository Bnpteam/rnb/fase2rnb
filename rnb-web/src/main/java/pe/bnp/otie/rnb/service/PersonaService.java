package pe.bnp.otie.rnb.service;
/**
 * Autor: Henrry Aris
 * Correo: ivan.hariasaqp@gmail.com
 * Fecha Creación:  -11-2019
 */
import pe.bnp.otie.rnb.dominio.Persona;

public interface PersonaService {
	
 	Persona buscarPersona(Persona persona);
 	
	Persona actualizar(Persona persona);
	
}
