package pe.bnp.otie.rnb.service;
/**
 * Autor: Henrry Aris Correo: ivan.hariasaqp@gmail.com Fecha Creación:
 * 15-11-2019
 */
import java.util.List;

import pe.bnp.otie.rnb.dominio.Utilitario;


public interface UtilitarioService {
	
	Utilitario crearUtilitario(Utilitario utilitario);

	List<Utilitario> listaUtilitario(Utilitario utilitario);
	
	List<Utilitario> listaUtilitario();
	
	Utilitario ocultarUtilitario(Utilitario utilitario);
	
	Utilitario editarUtilitario(Utilitario utilitario);
	
	Long cantidadUtilitario(Utilitario utilitario);
	
	List<Utilitario> listarSegunTipo(Long idTipoUtilitario);
	
}
