package pe.bnp.otie.rnb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.TipoBiblioteca;
import pe.bnp.otie.rnb.persistencia.TipoBibliotecaRepository;
import pe.bnp.otie.rnb.service.TipoBibliotecaService;

@Service("TipoBibliotecaService")
public class TipoBibliotecaServiceImpl implements TipoBibliotecaService {

	@Autowired
	TipoBibliotecaRepository tipoBibliotecaRepository;

	@Override
	public List<TipoBiblioteca> listaTipoBiblioteca(Long idTipoBiblioteca) {
		TipoBiblioteca tipoBiblioteca = new TipoBiblioteca();
		tipoBiblioteca.setIdTipoBiblioteca(idTipoBiblioteca);

		Example<TipoBiblioteca> example = Example.of(tipoBiblioteca);

		return tipoBibliotecaRepository.findAll(example);
	}

}
