package pe.bnp.otie.rnb.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.bnp.otie.rnb.dominio.TipoBiblioteca;

public interface TipoBibliotecaRepository extends JpaRepository<TipoBiblioteca, Long> {
	TipoBiblioteca findByIdTipoBiblioteca(Long idTipoBiblioteca);

}
