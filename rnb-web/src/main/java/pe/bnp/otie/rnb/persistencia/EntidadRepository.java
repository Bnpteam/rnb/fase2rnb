package pe.bnp.otie.rnb.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.bnp.otie.rnb.dominio.Entidad;

public interface EntidadRepository extends JpaRepository<Entidad, Long> {
	Entidad findByIdEntidad(Long idEntidad);

}
