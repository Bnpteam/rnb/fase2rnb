package pe.bnp.otie.rnb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.ActualizarMigracion;
import pe.bnp.otie.rnb.dominio.Responsable;
import pe.bnp.otie.rnb.service.ResponsableService;

@RestController
@RequestMapping("/responsable")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class ResponsableController {
	@Autowired
	private ResponsableService responsableService;

	@RequestMapping(value = "/actualizarResponble", method = RequestMethod.POST)
	public Responsable actualizarPersona(@RequestBody ActualizarMigracion actualizarMigracion) {
		return responsableService.actualizarResponsable(actualizarMigracion.getIdBiblioteca(),
				actualizarMigracion.getNombres(), actualizarMigracion.getApellidoPaterno(),
				actualizarMigracion.getApellidoMaterno(), actualizarMigracion.getNumeroDocumento());
	}
	
	@RequestMapping(value = "/getCorreoResponsable", method = RequestMethod.GET)	
	public List<Responsable> actualizarPersona() {
		return responsableService.getCorreoResponsable();
	}
}
