package pe.bnp.otie.rnb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.Persona;
import pe.bnp.otie.rnb.dominio.Responsable;
import pe.bnp.otie.rnb.persistencia.ResponsableRepository;
import pe.bnp.otie.rnb.service.PersonaService;
import pe.bnp.otie.rnb.service.ResponsableService;

@Service("ResponsableService")
public class ResponsableServiceImpl implements ResponsableService {
	@Autowired
	ResponsableRepository responsableRepository;

	@Autowired
	PersonaService personaService;

	@Override
	public Responsable buscarResponsable(Responsable responsable) {
		Example<Responsable> example = Example.of(responsable);
		return responsableRepository.findOne(example);
	}

	@Override
	public List<Responsable> buscarResponsableCorreo(Responsable responsable) {

		return responsableRepository.findByResponsableCorreo(responsable.getEstado());
	}

	@Override
	public Responsable actualizarResponsable(Long idBiblioteca, String nombres, String apellidoPaterno,
			String apellidoMaterno, String numerDocumento) {

		Persona persona = new Persona();
		Responsable responsable = new Responsable();
		try {
			responsable.setIdBiblioteca(idBiblioteca);

			responsable = buscarResponsable(responsable);

			persona.setIdPersona(responsable.getIdPersona());

			persona = personaService.buscarPersona(persona);

			persona.setNombres(nombres);
			persona.setApellidoPaterno(apellidoPaterno);
			persona.setApellidoMaterno(apellidoMaterno);
			persona.setNumerDocumento(numerDocumento);

			if (persona != null) {
				personaService.actualizar(persona);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return responsable;
	}

	@Override
	public List<Responsable> getCorreoResponsable() {

		Responsable responsable = new Responsable();
		responsable.setEstado("0");

		return buscarResponsableCorreo(responsable);
	}

	@Override
	public Boolean esResponsable(Long idPersona, Long idBiblioteca) {

		Responsable responsable = new Responsable();
		responsable.setIdBiblioteca(idBiblioteca);
		responsable.setIdPersona(idPersona);
		if (buscarResponsable(responsable) != null) {
			return true;
		}
		return false;
	}

	@Override
	public List<Responsable> listaResponsable(Responsable responsable) {
		Example<Responsable> example = Example.of(responsable);
		return responsableRepository.findAll(example);
	}

}
