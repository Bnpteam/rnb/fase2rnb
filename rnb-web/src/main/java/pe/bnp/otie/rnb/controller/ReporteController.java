package pe.bnp.otie.rnb.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.DocumentoRespuesta;
import pe.bnp.otie.rnb.service.ReporteService;

@RestController
@RequestMapping("/generarReporte")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class ReporteController {

	@Autowired
	private ReporteService reporteService;

	@RequestMapping(value = "/reportePorMes", method = RequestMethod.GET)
	public DocumentoRespuesta contarRegistradoresByDate(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date();

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return reporteService.generarReportePorMes(start1, end1);

	}
}
