package pe.bnp.otie.rnb.service;

/**
 * Autor: Henrry Aris
 * Correo: ivan.hariasaqp@gmail.com
 * Fecha Creación: 14-11-2019
 */
import java.util.List;

import pe.bnp.otie.rnb.dominio.Sugerencia;

public interface SugerenciaService {

	Sugerencia crearSugerencia(Sugerencia sugerencia);

	List<Sugerencia> listSugerencia();

	Sugerencia evaluarSugerencia(Sugerencia sugerencia);
	
	
}
