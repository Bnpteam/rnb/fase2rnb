package pe.bnp.otie.rnb.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.Registrador;
import pe.bnp.otie.rnb.persistencia.RegistradorRepository;
import pe.bnp.otie.rnb.service.RegistradorService;

@Service("RegistradorService")
public class RegistradorServiceImpl implements RegistradorService {

	@Autowired
	RegistradorRepository registradorRepository;

	@Override
	public List<Registrador> listarRegistrador() {
		return registradorRepository.findAll();
	}

	@Override
	public List<Registrador> listarRegistradorByDate(Date star, Date end) {
		return registradorRepository.findAllByFechaRegistroBetween(star, end);
	}

	@Override
	public Registrador buscarRegistrador(Registrador registrador) {

		Example<Registrador> example = Example.of(registrador);

		return registradorRepository.findOne(example);
	}

	@Override
	public Long contarRegistradorByDate(Date star, Date end) {
		return new Long(registradorRepository.findAllByFechaRegistroBetween(star, end).size());
	}

	@Override
	public Boolean esRegistrador(Registrador registrador) {

		if (buscarRegistrador(registrador) != null) {
			return true;
		} else {
			return false;
		}

	}

}
