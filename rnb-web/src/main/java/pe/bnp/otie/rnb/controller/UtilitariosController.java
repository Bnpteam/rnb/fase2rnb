package pe.bnp.otie.rnb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.Utilitario;
import pe.bnp.otie.rnb.service.UtilitarioService;

@RestController
@RequestMapping("/utilitario")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class UtilitariosController {

	@Autowired
	private UtilitarioService utilitarioService;

	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public Utilitario crearUtilitario(@RequestBody Utilitario utilitario) {
		return utilitarioService.crearUtilitario(utilitario);
	}

	@RequestMapping(value = "/ocultar", method = RequestMethod.POST)
	public Utilitario ocultarUtilitario(@RequestBody Utilitario utilitario) {
		return utilitarioService.ocultarUtilitario(utilitario);
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public Utilitario editarUtilitario(@RequestBody Utilitario utilitario) {
		return utilitarioService.editarUtilitario(utilitario);
	}

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public List<Utilitario> listaUtilitarios() {
		return utilitarioService.listaUtilitario();
	}
	
	@RequestMapping(value = "/cantidadUtilitario", method = RequestMethod.GET)
	public Long cantidadUtilitario() {
		Utilitario utilitario = new Utilitario();

		return utilitarioService.cantidadUtilitario(utilitario);
	}
	
	@RequestMapping(value = "/cantidadUtilitarioAtvicos", method = RequestMethod.GET)
	public Long cantidadUtilitarioActivos() {
		Utilitario utilitario = new Utilitario();
		utilitario.setEstado("1");
		return utilitarioService.cantidadUtilitario(utilitario);
	}
	
	@RequestMapping(value = "/cantidadUtilitarioOcultos", method = RequestMethod.GET)
	public Long cantidadUtilitarioOcultos() {
		Utilitario utilitario = new Utilitario();
		utilitario.setEstado("0");
		return utilitarioService.cantidadUtilitario(utilitario);
	}
		@RequestMapping(value = "/listaSegunTipo", method = RequestMethod.GET)
	public List<Utilitario> listaSegunTipo(@RequestParam(required = true) Long idTipoUtilitario) {

		return utilitarioService.listarSegunTipo(idTipoUtilitario);
	}
	

}
