package pe.bnp.otie.rnb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.dominio.Usuario;
import pe.bnp.otie.rnb.persistencia.UsuarioRepository;
import pe.bnp.otie.rnb.service.UsuarioService;

@Service("UsuarioService")
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public Boolean verificarUsuarioFirma(Long idUsuario) {
		Usuario usuarioSeleccionado = new Usuario();
		
		usuarioSeleccionado = usuarioRepository.findByIdUsuario(idUsuario);
		try {
		if(usuarioSeleccionado.getEstado().equals("1") && usuarioSeleccionado.getDisponible().equals("1")) {
			return true;
		}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	@Override
	public Usuario buscarUsuario(Usuario usuario) {
		Example<Usuario> example = Example.of(usuario);
		return usuarioRepository.findOne(example);
	}
	
	
	
}
