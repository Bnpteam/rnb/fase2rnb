package pe.bnp.otie.rnb.persistencia;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.dominio.Registrador;

@Repository
public interface BibliotecaMasterRepository extends JpaRepository<BibliotecaMaster, Long> {
	
	BibliotecaMaster findByBibliotecaMasterId(Long bibliotecaMasterId);

	@Query("select b from  BibliotecaMaster b where b.column1 != :estadoFirma" )
	List<BibliotecaMaster> findByBibliotecaMasterNotQuery(@Param("estadoFirma") String estadoFirma);
	
	@Query("select a from BibliotecaMaster a where a.column1 = '1' and   a.fechaRegistro between :start and :end" )
    List<BibliotecaMaster> getConstanciasEmitidasByFechaRegistro(@Param("start") Date start,  @Param("end") Date end);
		
 }
