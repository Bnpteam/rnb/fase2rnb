package pe.bnp.otie.rnb.service;

import java.util.Date;
import java.util.List;

import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.dominio.DocumentoRespuesta; 

public interface BibliotecaMasterService {

	List<BibliotecaMaster> listar();

	List<BibliotecaMaster> listarFirma();

	BibliotecaMaster listar(BibliotecaMaster bibliotecaMaster);

	BibliotecaMaster buscarBiblioteca(BibliotecaMaster bibliotecaMaster);

	BibliotecaMaster consultarBibliotecaMaster(Long idBibliotecaMaster);

	DocumentoRespuesta reporteBibliotecaMaster(Long idBibliotecaMaster);

	BibliotecaMaster actualizar(BibliotecaMaster bibliotecaMaster);

	DocumentoRespuesta getOpenKmBibliotecaMaster(String numeroRNB, String departamento, String provincia);
	
	Integer cantidadBibliotecasVerificadas();
	
	List<BibliotecaMaster> listarPorCampo(BibliotecaMaster bibliotecaMaster);
	
	Integer bibliotecasConFirma();
	
	List<BibliotecaMaster> listarConstanciasEmitidas(Date start, Date end);
	
	Long contarConstanciasEmitidas(Date start, Date end); 
	
	Long contarConstanciasFaltantes(Date start, Date end);
	  
	Boolean esBibliotecaMaster(BibliotecaMaster bibliotecaMaster);
	
 	List<BibliotecaMaster> buscarBMPorRegistrador(Long idRegistrador);
 }
