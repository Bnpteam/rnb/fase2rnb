package pe.bnp.otie.rnb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.Entidad;
import pe.bnp.otie.rnb.dominio.EntidadBiblioteca;
import pe.bnp.otie.rnb.persistencia.EntidadBibliotecaRepository;
import pe.bnp.otie.rnb.service.EntidadBibliotecaService;

@Service("EntidadBibliotecaService")
public class EntidadBibliotecaServiceImpl implements EntidadBibliotecaService	 {
	
	@Autowired
	EntidadBibliotecaRepository entidadBibliotecaRepository;
	
	
	@Override
	public EntidadBiblioteca buscarEntidad(EntidadBiblioteca entidadBiblioteca) {

		// Example<Entidad> example = Example.of(entidadBiblioteca);

		// return entidadBibliotecaRepository.findOne(example);
		return null;
	}
	
	@Override
	public EntidadBiblioteca actualizar(EntidadBiblioteca entidadBiblioteca) {
		// TODO Auto-generated method stub
		return entidadBibliotecaRepository.save(entidadBiblioteca);
		}

}
