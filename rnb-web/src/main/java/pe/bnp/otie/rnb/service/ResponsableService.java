package pe.bnp.otie.rnb.service;
/**
 * Autor: Henrry Aris
 * Correo: ivan.hariasaqp@gmail.com
 * Fecha Creación:  -10-2019
 */
import java.util.List;

import pe.bnp.otie.rnb.dominio.Responsable;

public interface ResponsableService {
	
	Responsable buscarResponsable(Responsable responsable);

	Responsable actualizarResponsable(Long idBiblioteca, String nombres, String apellidoPaterno, String apellidoMaterno, String numerDocumento);
	
	List<Responsable> buscarResponsableCorreo(Responsable responsable);
	
	List<Responsable> getCorreoResponsable();
	
	Boolean esResponsable(Long idPersona, Long idBiblioteca);
		
	List<Responsable> listaResponsable(Responsable responsable);
}
