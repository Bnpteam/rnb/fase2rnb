package pe.bnp.otie.rnb.service;

import pe.bnp.otie.rnb.dominio.Entidad;

public interface EntidadService {

	Entidad actualizarEntidad(String ruc, String rucNuevo, String razonSocial, String direccion, Long idBiblioteca);

	Entidad buscarEntidad(Entidad entidad);
	
	Entidad actualizar(Entidad entidad	);

}
