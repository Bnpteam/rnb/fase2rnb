package pe.bnp.otie.rnb.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.Responsable;
import pe.bnp.otie.rnb.dominio.Sugerencia;
import pe.bnp.otie.rnb.service.ResponsableService;
import pe.bnp.otie.rnb.service.SugerenciaService;

@RestController
@RequestMapping("/sugerencia")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class SugerenciaController {
	@Autowired
	private SugerenciaService sugerenciaService;
 

	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public Sugerencia crearSugerencia(@RequestBody Sugerencia sugerencia) { 

		
		return sugerenciaService.crearSugerencia(sugerencia);
	}

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public List<Sugerencia> listaMensaje() {
		return sugerenciaService.listSugerencia();
	}

	@RequestMapping(value = "/evaluar", method = RequestMethod.POST)
	public Sugerencia evaluarSugerencia(@RequestBody Sugerencia sugerencia) {
		return sugerenciaService.evaluarSugerencia(sugerencia);
	}

}
