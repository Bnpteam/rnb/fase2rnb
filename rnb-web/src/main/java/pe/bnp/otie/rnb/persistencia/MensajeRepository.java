package pe.bnp.otie.rnb.persistencia;
/**
 * Autor: Henrry Aris
 * Correo: ivan.hariasaqp@gmail.com
 * Fecha Creación: 14-11-2019
 */
import org.springframework.data.jpa.repository.JpaRepository;

import pe.bnp.otie.rnb.dominio.Mensaje;

public interface MensajeRepository extends JpaRepository<Mensaje, Long> {
	Mensaje findByIdMensaje(Long idMensaje);

}
