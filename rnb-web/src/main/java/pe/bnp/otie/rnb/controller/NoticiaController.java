package pe.bnp.otie.rnb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.Noticia;
import pe.bnp.otie.rnb.service.NoticiaService;

@RestController
@RequestMapping("/noticia")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class NoticiaController {

	@Autowired
	NoticiaService noticiaService;

	@RequestMapping(method = RequestMethod.GET, path = "/healthCheck")
	public String healthCheck(){ 
		return "api backend active!"; 
	}	
	/**
	 * los eventos son considerados noticias estas pueden ser de distintos tipos
	 */
	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public Noticia crearEvento(@RequestBody Noticia noticia) {
		return noticiaService.crearNoticia(noticia);
	}

	@RequestMapping(value = "/listaPorTipo", method = RequestMethod.GET)
	public List<Noticia> listaPorTipo(@RequestParam(required = true) Long idtipoBiblioteca) {
		Noticia noticia = new Noticia();
		noticia.setEstado("1");
		noticia.setIdtipoBiblioteca(idtipoBiblioteca);
		return noticiaService.listaNoticiaTipo(noticia);
	}

	@RequestMapping(value = "/noticiaPorTipo", method = RequestMethod.GET)
	public List<Noticia> noticiaPorTipo(@RequestParam(required = true) Long idtipoBiblioteca) {

		return noticiaService.listTipoBibliotecaNoticias(idtipoBiblioteca);
	}

	@RequestMapping(value = "/listaPorBiblioteca", method = RequestMethod.GET)
	public List<Noticia> listaBiblioteca(@RequestParam(required = true) Long tipoEventoId) {
		Noticia noticia = new Noticia();
		noticia.setEstado("1");
		noticia.setTipoEventoId(tipoEventoId);
		return noticiaService.listaNoticiaTipo(noticia);
	}

	@RequestMapping(value = "/listaFragmento", method = RequestMethod.GET)
	public List<Noticia> listaNoticiaPorBiblitoeca(@RequestParam(required = true) Long tipoEventoId,
			@RequestParam(required = true) Long idBiblioteca) {
		return noticiaService.listaNoticia(tipoEventoId, idBiblioteca);
	}

	@RequestMapping(value = "/eventoPorDepartamento", method = RequestMethod.GET)
	public List<Noticia> eventoPorDepartamento(@RequestParam(required = true) String departamento) {
		return noticiaService.listaNoticiaPorDepartamento(departamento);
	}

}
