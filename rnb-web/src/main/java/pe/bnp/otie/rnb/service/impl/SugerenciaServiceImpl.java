package pe.bnp.otie.rnb.service.impl;
/**
 * Autor: Henrry Aris
 * Correo: ivan.hariasaqp@gmail.com
 * Fecha Creación: 14-11-2019
 */

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.Registrador;
import pe.bnp.otie.rnb.dominio.Sugerencia;
import pe.bnp.otie.rnb.persistencia.SugerenciaRepository;
import pe.bnp.otie.rnb.service.RegistradorService;
import pe.bnp.otie.rnb.service.ResponsableService;
import pe.bnp.otie.rnb.service.SugerenciaService;

@Service("SugerenciaService")
public class SugerenciaServiceImpl implements SugerenciaService{

	@Autowired
	SugerenciaRepository sugerenciaRepository;
	
	@Autowired
	ResponsableService responsableService;
	
	@Autowired
	RegistradorService registradorService;	
	
	@Override
	public Sugerencia crearSugerencia(Sugerencia sugerencia) {
		
		sugerencia.setFechaCarga(new Date());
		sugerencia.setFechaRegistro(new Date());
		
		Registrador registrador = new Registrador();
		registrador.setIdRegistrador(sugerencia.getIdPersona());
		registrador = registradorService.buscarRegistrador(registrador);
//		Responsable responsable = new Responsable();
//		responsable.setIdResponsable(sugerencia.);
		/** obtener el idPersona*/
		//responsable = responsableService.buscarResponsable(registrador.get);
 
		sugerencia.setIdPersona(registrador.getIdPersona());
		
		
		Date fechaRegistro = new Date();
		
		/**las sugerencias se crean con estado inicial 0*/
		String estado = "0";
		
		sugerencia.setFechaRegistro(fechaRegistro);
		sugerencia.setEstado(estado);
		
		return sugerenciaRepository.save(sugerencia);
	}

	@Override
	public List<Sugerencia> listSugerencia() {
 		return sugerenciaRepository.findAll();
	}

	@Override
	public Sugerencia evaluarSugerencia(Sugerencia sugerencia) {
		String estado="1";
		sugerencia.setFechaModificacion(new Date());
		sugerencia.setEstado(estado);
		
 		return sugerenciaRepository.save(sugerencia);
	}

 

}
