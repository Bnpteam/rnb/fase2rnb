package pe.bnp.otie.rnb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.Biblioteca;
import pe.bnp.otie.rnb.dominio.Entidad;
import pe.bnp.otie.rnb.dominio.EntidadBiblioteca;
import pe.bnp.otie.rnb.persistencia.EntidadRepository;
import pe.bnp.otie.rnb.service.BibliotecaService;
import pe.bnp.otie.rnb.service.EntidadBibliotecaService;
import pe.bnp.otie.rnb.service.EntidadService;

@Service("EntidadService")
public class EntidadServiceImpl implements EntidadService {

	@Autowired
	EntidadRepository entidadRepository;
	
	@Autowired
	BibliotecaService bibliotecaService;
	
	@Autowired
	EntidadBibliotecaService entidadBibliotecaService;
	
	@Override
	public Entidad buscarEntidad(Entidad entidad) {

		Example<Entidad> example = Example.of(entidad);

		return entidadRepository.findOne(example);
	}

	@Override
	public Entidad actualizar(Entidad entidad) {
		return entidadRepository.save(entidad);
	}

	@Override
	public Entidad actualizarEntidad(String ruc, String rucNuevo, String razonSocial, String direccion, Long idBiblioteca) {
		
		Entidad entidadTemp = new Entidad();
		Entidad entidadTempActual = new Entidad();
		Biblioteca biblioteca = new Biblioteca();
		
		EntidadBiblioteca entidadBiblioteca =  new EntidadBiblioteca();
		
		try {	
			entidadTemp.setRuc(ruc);
			entidadTemp = buscarEntidad(entidadTemp);
			
			entidadTempActual = buscarEntidad(entidadTemp);
 			entidadTemp.setRuc(rucNuevo);
			entidadTemp.setRazonSocial(razonSocial);
			entidadTemp.setFlagOrigen("SUNAT"); 
			
 			
		//	biblioteca = bibliotecaService.buscarBibliotecaOrigen(idBiblioteca);
			
			actualizar(entidadTemp);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return entidadTemp;
	}

}
