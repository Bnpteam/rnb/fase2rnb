package pe.bnp.otie.rnb.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import pe.bnp.otie.rnb.dominio.Biblioteca;
import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.dominio.DocumentoRespuesta;
import pe.bnp.otie.rnb.dominio.Registrador;
import pe.bnp.otie.rnb.dominio.Responsable;
import pe.bnp.otie.rnb.persistencia.BibliotecaMasterRepository;
import pe.bnp.otie.rnb.persistencia.BibliotecaRepository;
import pe.bnp.otie.rnb.service.BibliotecaMasterService;
import pe.bnp.otie.rnb.service.BibliotecaService;
import pe.bnp.otie.rnb.service.RegistradorService;
import pe.bnp.otie.rnb.service.ResponsableService;
import pe.gob.bnp.tramitedoc.util.FileOpenKM;
import pe.gob.bnp.tramitedoc.util.OpenKMDocumento;
import pe.gob.bnp.tramitedoc.util.ResultOpenKMDocumento;
import pe.gob.bnp.tramitedoc.util.VO;

@Service("bibliotecaMasterService")
public class BibliotecaMasterServiceImpl implements BibliotecaMasterService {

	public static final String CARPETA_RAIZ = "/E_GESDOC";
	public static final String OPERACION_EXITO = "00000";

	@Autowired
	BibliotecaMasterRepository bibliotecaMasterRepository;

	@Autowired
	BibliotecaRepository bibliotecaRepository;

	@Autowired
	BibliotecaService bibliotecaService;

	@Autowired
	ResponsableService responsableService;

	@Autowired
	RegistradorService registradorService;
	

	@Override
	public List<BibliotecaMaster> listar() {
		return bibliotecaMasterRepository.findAll();
	}

	@Override
	public List<BibliotecaMaster> listarFirma() {

		// return bibliotecaMasterRepository.findByBibliotecaMasterNotQuery("3");

		return bibliotecaMasterRepository.findAll();
	}

	@Override
	public List<BibliotecaMaster> listarPorCampo(BibliotecaMaster bibliotecaMaster) {

		Example<BibliotecaMaster> example = Example.of(bibliotecaMaster);
		return bibliotecaMasterRepository.findAll(example);
	}

	@Override
	public BibliotecaMaster listar(BibliotecaMaster biblioteca) {
		return bibliotecaMasterRepository.findByBibliotecaMasterId(biblioteca.getBibliotecaMasterId());

	}

	@Override
	public BibliotecaMaster consultarBibliotecaMaster(Long idBibliotecaMaster) {
		// TODO Auto-generated method stub
		return bibliotecaMasterRepository.findByBibliotecaMasterId(idBibliotecaMaster);
	}

	@Override
	public DocumentoRespuesta reporteBibliotecaMaster(Long idBibliotecaMaster) {

		String encodedString = null;
		DocumentoRespuesta documentoRespuesta = new DocumentoRespuesta();

		try {

			BibliotecaMaster bibliotecaSeleccionada = new BibliotecaMaster();

			bibliotecaSeleccionada = consultarBibliotecaMaster(idBibliotecaMaster);

			File jasperFile = ResourceUtils.getFile("classpath:jasper/Constancia.jrxml");

			Map<String, Object> parameters = new HashMap<String, Object>();

			File pdfFile = jasperFile.getParentFile();

			final JasperDesign jd = JRXmlLoader.load(jasperFile);

			final JasperReport jasperReport = JasperCompileManager.compileReport(jd);
			// extraer la direccion
			// JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperFile);

			Biblioteca biblioteca = new Biblioteca();

			biblioteca.setIdBiblioteca(bibliotecaSeleccionada.getBibliotecaIdOrig());

			biblioteca = bibliotecaRepository.findByIdBiblioteca(bibliotecaSeleccionada.getBibliotecaIdOrig());

			Responsable responsable = new Responsable();
			responsable.setIdBiblioteca(biblioteca.getIdBiblioteca());
			responsable = responsableService.buscarResponsable(responsable);
			/*linux*/
			// String direccionPdf = "//opt//tomcat//webapps//rnb//carpeta//";
			/* windows*/
			String direccionPdf = "/opt/tomcat/webapps/rnb/carpeta/";
			parameters.clear();
			parameters.put("codRnb", bibliotecaSeleccionada.getNumeroRnb());
			parameters.put("nombreBiblioteca", bibliotecaSeleccionada.getNombre());
			parameters.put("direccionBiblioteca", biblioteca.getDireccion());
			parameters.put("distritoBiblioteca", bibliotecaSeleccionada.getDistrito());
			parameters.put("provinciaBiblioteca", bibliotecaSeleccionada.getProvincia());
			parameters.put("departamentoBiblioteca", bibliotecaSeleccionada.getDepartamento());
			// parameters.put("imagenCabecera",
			// this.getClass().getResource("http://172.16.88.115:8080/rnb/carpeta/logoCultura.png"));

			String nombreConstancia = bibliotecaSeleccionada.getNumeroRnb();
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(jasperPrint,
			// "D:/opt/tomcat/webapps/rnb/carpeta" + "/" + idBibliotecaMaster + ".pdf");
			JasperExportManager.exportReportToPdfFile(jasperPrint, direccionPdf + idBibliotecaMaster + ".pdf");

			// pdfFile.getAbsolutePath() + "/" + nombreConstancia + ".pdf");

			// File enviarPdf = ResourceUtils.getFile(pdfFile.getAbsolutePath() + "/" +
			// nombreConstancia + ".pdf");
			File enviarPdf = ResourceUtils.getFile(direccionPdf + idBibliotecaMaster + ".pdf");
//			 File enviarPdf = ResourceUtils.getFile("\\usr\\share\\rnb\\" +
//			 nombreConstancia + ".pdf");
			encodedString = Base64.getEncoder().encodeToString(Files.readAllBytes(enviarPdf.toPath()));

			ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
			OpenKMDocumento openKMDocumento = new OpenKMDocumento();
			FileOpenKM fileOpenKM = new FileOpenKM();

			fileOpenKM.setFileName(nombreConstancia + ".pdf");
			fileOpenKM.setFechaDocumento(new Date());
			fileOpenKM.setRawFile(Files.readAllBytes(enviarPdf.toPath()));
			fileOpenKM.setExtension("pdf");

			// resultOpenKMDocumento = openKMDocumento.registrarContenido(fileOpenKM);

			if (!VO.isNull(resultOpenKMDocumento)) {
				if (resultOpenKMDocumento.getCodigo().equals(OPERACION_EXITO)) {
					System.out.print(resultOpenKMDocumento.getUiid());
				}
			}

			byte[] strToBytes = Files.readAllBytes(enviarPdf.toPath());
//			outputStream.write(strToBytes);
//
//			outputStream.close();

			if (strToBytes != null && !direccionPdf.equals("")) {
				Files.write(new File(direccionPdf + idBibliotecaMaster.toString() + ".pdf").toPath(), strToBytes);
			}

			// JasperViewer.viewReport(jasperPrint, false);

			documentoRespuesta.setNombreDocumento("La operación finalizaco sin inconvenientes ");
			documentoRespuesta.setDataDocumento(encodedString);
			documentoRespuesta.setNombreBiblioteca(biblioteca.getNombre());
			documentoRespuesta.setAnio(biblioteca.getAnioDeclaracion());
			documentoRespuesta.setEmailDestino(responsable.getCorreo());

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// log.error("Error: " + e.getMessage(), e);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return documentoRespuesta;
	}

	@Override
	public BibliotecaMaster actualizar(BibliotecaMaster bibliotecaMaster) {
		// TODO Auto-generated method stub
		return bibliotecaMasterRepository.save(bibliotecaMaster);
	}

	@Override
	public BibliotecaMaster buscarBiblioteca(BibliotecaMaster bibliotecaMaster) {

		Example<BibliotecaMaster> example = Example.of(bibliotecaMaster);

		return bibliotecaMasterRepository.findOne(example);
		// return bibliotecaMasterRepository.findAll(example);
	}

	@Override
	public DocumentoRespuesta getOpenKmBibliotecaMaster(String numeroRNB, String departamento, String provincia) {

		DocumentoRespuesta documentoRespuesta = new DocumentoRespuesta();

		BibliotecaMaster bibliotecaSeleccionada = new BibliotecaMaster();
		try {
			bibliotecaSeleccionada.setNumeroRnb(numeroRNB);
			bibliotecaSeleccionada.setDepartamento(departamento);
			bibliotecaSeleccionada.setProvincia(provincia);

			bibliotecaSeleccionada = buscarBiblioteca(bibliotecaSeleccionada);
			Biblioteca biblioteca = new Biblioteca();

			biblioteca.setIdBiblioteca(bibliotecaSeleccionada.getBibliotecaIdOrig());

			biblioteca = bibliotecaRepository.findByIdBiblioteca(bibliotecaSeleccionada.getBibliotecaIdOrig());
			Responsable responsable = new Responsable();
			responsable.setIdBiblioteca(biblioteca.getIdBiblioteca());
			responsable = responsableService.buscarResponsable(responsable);

			ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
			OpenKMDocumento openKMDocumento = new OpenKMDocumento();
			FileOpenKM fileOpenKM = new FileOpenKM();

			fileOpenKM.setUiid(bibliotecaSeleccionada.getColumn2());

			resultOpenKMDocumento = openKMDocumento.consultarContenido(fileOpenKM);

			documentoRespuesta.setNombreDocumento(bibliotecaSeleccionada.getNumeroRnb());
			documentoRespuesta.setDataDocumento(Base64.getEncoder().encodeToString(resultOpenKMDocumento.getRawFile()));
			documentoRespuesta.setNombreBiblioteca(biblioteca.getNombre());
			documentoRespuesta.setAnio(biblioteca.getAnioDeclaracion());
			documentoRespuesta.setEmailDestino(responsable.getCorreo());
			documentoRespuesta.setFlag(bibliotecaSeleccionada.getColumn1());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return documentoRespuesta;
	}

	@Override
	public Integer cantidadBibliotecasVerificadas() {
		int cantidadBibliotecas = 0;
		cantidadBibliotecas = listar().size();
		return cantidadBibliotecas;
	}

	@Override
	public Integer bibliotecasConFirma() {
		BibliotecaMaster bibliotecaMaster = new BibliotecaMaster();
		bibliotecaMaster.setColumn1("1");
		return listarPorCampo(bibliotecaMaster).size();
	}

	@Override
	public List<BibliotecaMaster> listarConstanciasEmitidas(Date start, Date end) {
		return bibliotecaMasterRepository.getConstanciasEmitidasByFechaRegistro(start, end);
	}

	@Override
	public Long contarConstanciasEmitidas(Date start, Date end) {
		return new Long(listarConstanciasEmitidas(start, end).size());
	}

	@Override
	public Long contarConstanciasFaltantes(Date start, Date end) {
		try {
			return new Long(bibliotecaMasterRepository.findByBibliotecaMasterNotQuery("1").size());
		} catch (Exception e) {

		}
		return new Long(-1);
	}

	@Override
	public Boolean esBibliotecaMaster(BibliotecaMaster bibliotecaMaster) {

		if (buscarBiblioteca(bibliotecaMaster) != null) {
			return true;
		} else {
			return null;
		}
	}

	@Override
	public List<BibliotecaMaster> buscarBMPorRegistrador(Long idRegistrador) {

 		List<BibliotecaMaster> bibliotecaMasters = new ArrayList<BibliotecaMaster>();
		List<Responsable> responsableBibliotecas = new ArrayList<Responsable>();
		
		Responsable responsable = new Responsable();
 
 		Registrador registrador = new Registrador();
		registrador.setIdRegistrador(idRegistrador);

		registrador = registradorService.buscarRegistrador(registrador);

		Long idPersona = registrador.getIdPersona();
		
		responsable.setIdPersona(idPersona);
		
		responsableBibliotecas = responsableService.listaResponsable(responsable);

		for (int i = 0; i < responsableBibliotecas.size(); i++) {
			responsable = responsableBibliotecas.get(i);
			BibliotecaMaster bibliotecaMaster = new BibliotecaMaster();

			bibliotecaMaster.setBibliotecaIdOrig(responsable.getIdBiblioteca());

			if (esBibliotecaMaster(bibliotecaMaster)) {
				if (responsableService.esResponsable(idPersona, responsable.getIdBiblioteca())) {
					bibliotecaMaster = buscarBiblioteca(bibliotecaMaster);
					bibliotecaMasters.add(bibliotecaMaster);
				}
			}

			bibliotecaMaster = null;
 		}

		return bibliotecaMasters;
	}

}
