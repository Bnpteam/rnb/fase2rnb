package pe.bnp.otie.rnb.service;

import java.util.List;

import pe.bnp.otie.rnb.dominio.TablaMaestra;

public interface TablaMaestraService {

	List<TablaMaestra> listarSubTabla(TablaMaestra tipoTabla);
	
}
