package pe.bnp.otie.rnb.persistencia;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.bnp.otie.rnb.dominio.Noticia;

public interface NoticiaRepository extends JpaRepository<Noticia, Long> {

	Noticia findByIdNoticia(Long idNoticia);

	@Query("select distinct (a.nombreBiblioteca), a.idBiblioteca from Noticia a where a.estado = '1' and a.tipoEventoId = :tipoEventoId and a.fechaFin >= :fechaEvento")
	List<Noticia> getNoticias(@Param("fechaEvento") Date fechaEvento, @Param("tipoEventoId") Long tipoEventoId);

	@Query("select a from Noticia a where a.estado = '1' and a.idtipoBiblioteca = :idtipoBiblioteca and a.fechaFin >= :fechaEvento")
	List<Noticia> getNoticiasTipoBiblioteca(@Param("fechaEvento") Date fechaEvento, @Param("idtipoBiblioteca") Long idtipoBiblioteca);
	
	
	@Query("select a from Noticia a where a.estado = '1' and a.idBiblioteca=:idBiblioteca and a.tipoEventoId = :tipoEventoId and a.fechaFin >= :fechaEvento")
	List<Noticia> getNoticiasPorBiblioteca(@Param("fechaEvento") Date fechaEvento, @Param("tipoEventoId") Long tipoEventoId,  @Param("idBiblioteca") Long idBiblioteca);
	
	@Query("select a from Noticia a where a.estado = '1' and a.departamento = :departamento and a.fechaFin >= :fechaEvento")
	List<Noticia> getNoticiaPorDepartamento(@Param("fechaEvento") Date fechaEvento, @Param("departamento") String departamento  );
	
}
