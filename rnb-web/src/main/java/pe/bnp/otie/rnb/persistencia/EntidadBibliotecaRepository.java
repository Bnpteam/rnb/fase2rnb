package pe.bnp.otie.rnb.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

 import pe.bnp.otie.rnb.dominio.EntidadBiblioteca;

public interface EntidadBibliotecaRepository  extends JpaRepository<EntidadBiblioteca, Long> {
	EntidadBiblioteca findByIdEntidad(Long EntidadBiblioteca);
}
