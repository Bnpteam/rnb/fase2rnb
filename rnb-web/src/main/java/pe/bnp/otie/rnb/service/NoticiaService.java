package pe.bnp.otie.rnb.service;

import java.util.Date;
import java.util.List;

import pe.bnp.otie.rnb.dominio.Noticia;

public interface NoticiaService {

	Noticia crearNoticia(Noticia noticia);

	List<Noticia> listaNoticias();

	List<Noticia> listaNoticias(Noticia noticia);

	Noticia aprobarNoticia(Noticia noticia);

	List<Noticia> listaNoticiaTipo(Noticia noticia);

	List<Noticia> listaNoticia(Long tipoEventoId, Long idBiblioteca);

	List<Noticia> listaNoticiaPorFecha(Long tipoEventoId, Date fechaEvento);
	
	List<Noticia> listaNoticiaPorDepartamento(String departamento);
	
	List<Noticia> listTipoBibliotecaNoticias(Long idtipoBiblioteca);
}
