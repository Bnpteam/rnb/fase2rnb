package pe.bnp.otie.rnb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.ActualizarMigracion;
import pe.bnp.otie.rnb.dominio.Biblioteca;
import pe.bnp.otie.rnb.dominio.Entidad;
import pe.bnp.otie.rnb.dominio.Persona;
import pe.bnp.otie.rnb.persistencia.PersonaRepository;
import pe.bnp.otie.rnb.service.BibliotecaService;
import pe.bnp.otie.rnb.service.PersonaService;

@Service("PersonaService")
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	PersonaRepository personaRepository;

	@Autowired
	BibliotecaService bibliotecaService;

	@Override
	public Persona buscarPersona(Persona persona) {

		Example<Persona> example = Example.of(persona);

		return personaRepository.findOne(example);
	}

	@Override
	public Persona actualizar(Persona persona) {
		return personaRepository.save(persona);
	}

	 

}
