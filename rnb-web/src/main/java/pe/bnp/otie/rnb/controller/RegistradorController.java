package pe.bnp.otie.rnb.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.Registrador;
import pe.bnp.otie.rnb.service.RegistradorService;

@RestController
@RequestMapping("/registrador")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class RegistradorController {
	@Autowired
	private RegistradorService registradorService;

	@RequestMapping(value = "/listarRegistrador", method = RequestMethod.GET)
	public List<Registrador> listarRegistrador() {
		return registradorService.listarRegistrador();
	}

	@RequestMapping(value = "/listarRegistradorByDate", method = RequestMethod.GET)
	public List<Registrador> listarRegistradorPorFecha(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date(); 

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return registradorService.listarRegistradorByDate(start1, end1);

	}
	
		@RequestMapping(value = "/contarRegistradoresByDate", method = RequestMethod.GET)
	public Long contarRegistradoresByDate(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date(); 

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return registradorService.contarRegistradorByDate(start1, end1);

	}
	
}
