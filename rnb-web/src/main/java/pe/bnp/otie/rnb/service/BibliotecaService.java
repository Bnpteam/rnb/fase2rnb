package pe.bnp.otie.rnb.service;

import java.util.Date;
import java.util.List;

import pe.bnp.otie.rnb.dominio.Biblioteca;

public interface BibliotecaService {

	Biblioteca buscarBibliotecaOrigen(Long idBiblioteca);
	
	Biblioteca buscarBiblioteca(Long idBiblioteca);
	
	Biblioteca actualizarRegistrador(Long idBiblioteca, Long idRegistrador);
	
	Long cantidadSolicitudBiblioteca();
	
	List<Biblioteca> listarBiblioteca (Biblioteca biblioteca);
	
	List<Biblioteca> listarBibliotecaByDate(Date star, Date end);
	
	Long contarBibliotecaByDate(Date star, Date end);
	
	List<Biblioteca> buscarBibliotecaPorRegistrador(Long idRegistrador);
	
 }
