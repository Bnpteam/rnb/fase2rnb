package pe.bnp.otie.rnb.persistencia;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.bnp.otie.rnb.dominio.Registrador;

public interface RegistradorRepository extends JpaRepository<Registrador, Long> {
	
	Registrador findByIdRegistrador(Long idRegistrador);
	
	List<Registrador> findAllByFechaRegistroBetween(Date star, Date end);
	
}
