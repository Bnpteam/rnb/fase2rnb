package pe.bnp.otie.rnb.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.dominio.DocumentoRespuesta;
import pe.bnp.otie.rnb.service.BibliotecaMasterService;

@RestController
@RequestMapping("/bibliotecaMaster")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class BibliotecaMasterController {

	@Autowired
	private BibliotecaMasterService biblitoecaMasterService;

	@RequestMapping(value = "/consultar", method = RequestMethod.GET)
	public List<BibliotecaMaster> listarBibliotecaMaster(
			@RequestParam(name = "columna1", required = false) String column1,
			@RequestParam(required = false) String nombre) {

		return biblitoecaMasterService.listarFirma();
	}

	@RequestMapping(value = "/consultarFirma", method = RequestMethod.GET)
	public BibliotecaMaster listarBibliotecaMasterFirma(
			@RequestParam(name = "columna1", required = false) String column1,
			@RequestParam(required = false) String nombre, @RequestParam(required = false) String numeroRnb,
			@RequestParam(required = false) String departamento, @RequestParam(required = false) String provincia) {

		BibliotecaMaster bibliotecaMaster = new BibliotecaMaster();
		bibliotecaMaster.setNumeroRnb(numeroRnb);
		bibliotecaMaster.setDepartamento(departamento);
		bibliotecaMaster.setProvincia(provincia);
		return biblitoecaMasterService.buscarBiblioteca(bibliotecaMaster);
	}

	@RequestMapping(value = "/reporte", method = RequestMethod.GET)
	public DocumentoRespuesta reporteBibliotecaMaster(
			@RequestParam(name = "bibliotecaMasterId", required = true) Long idBibliotecaMaster) {

		return biblitoecaMasterService.reporteBibliotecaMaster(idBibliotecaMaster);
	}

	@RequestMapping(value = "/constanciaDocumento", method = RequestMethod.GET)
	public DocumentoRespuesta getConstanciaBibliotecaMaster(@RequestParam(required = false) String numeroRnb,
			@RequestParam(required = false) String departamento, @RequestParam(required = false) String provincia) {

		return biblitoecaMasterService.getOpenKmBibliotecaMaster(numeroRnb, departamento, provincia);
	}

	@RequestMapping(value = "/masterBibliotecaConsulta/{codMasterBiblioteca}", method = RequestMethod.GET)
	public BibliotecaMaster consultarBibliotecaMaster(@PathVariable Long codMasterBiblioteca) {

		return biblitoecaMasterService.consultarBibliotecaMaster(codMasterBiblioteca);
	}

	@RequestMapping(value = "/cantidadBibliotecasV", method = RequestMethod.GET)
	public Integer consultarBibliotecaMaster() {
		return biblitoecaMasterService.cantidadBibliotecasVerificadas();
	}

	@RequestMapping(value = "/constanciasEmitidas", method = RequestMethod.GET)
	public List<BibliotecaMaster> constanciasEmitidasBM(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date();

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return biblitoecaMasterService.listarConstanciasEmitidas(start1, end1);
	}

	@RequestMapping(value = "/contarConstanciasEmitidas", method = RequestMethod.GET)
	public Long contarConstanciasEmitidasBM(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date();

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return biblitoecaMasterService.contarConstanciasEmitidas(start1, end1);
	}

	@RequestMapping(value = "/contarConstanciasSinFirma", method = RequestMethod.GET)
	public Long contarConstanciasSinFirma(@RequestParam(required = false) String start,
			@RequestParam(required = false) String end) {

		DateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
		Date start1 = new Date();
		Date end1 = new Date();

		try {
			start1 = dateFormate.parse(start);
			end1 = dateFormate.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return biblitoecaMasterService.contarConstanciasFaltantes(start1, end1);
	}

	@RequestMapping(value = "/listaBMPorRegistrador", method = RequestMethod.GET)
	public List<BibliotecaMaster> listaPorRegistrador(@RequestParam(required = false) Long idRegistrador) {

		return biblitoecaMasterService.buscarBMPorRegistrador(idRegistrador);
	}

	@RequestMapping(value = "/buscarBM", method = RequestMethod.GET)
	public BibliotecaMaster buscarBM(@RequestParam(required = true) Long bibliotecaMasterId) {
		BibliotecaMaster bibliotecaMaster = new BibliotecaMaster();

		bibliotecaMaster.setBibliotecaMasterId(bibliotecaMasterId);
		return biblitoecaMasterService.buscarBiblioteca(bibliotecaMaster);
	}

}
