package pe.bnp.otie.rnb.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.dominio.DocumentoRespuesta;
import pe.bnp.otie.rnb.service.BibliotecaMasterService;
import pe.bnp.otie.rnb.service.BibliotecaService;
import pe.bnp.otie.rnb.service.RegistradorService;
import pe.bnp.otie.rnb.service.ReporteService;

@Service("ReporteService")
public class ReporteServiceImpl implements ReporteService {

	@Autowired
	BibliotecaService bibliotecaService;

	@Autowired
	BibliotecaMasterService bibliotecaMasterService;

	@Autowired
	RegistradorService registradorService;

	@Override
	public DocumentoRespuesta generarReportePorMes(Date start, Date end) {

		String encodedString = null;
		DocumentoRespuesta documentoRespuesta = new DocumentoRespuesta();

		Long cantidadBiblioteca = new Long(0);
		Long cantidadRegistrador = new Long(0);
		Long cantidadConstancias = new Long(0);

		try {

			File jasperFile = ResourceUtils.getFile("classpath:jasper/reporteMes.jrxml");

			Map<String, Object> parameters = new HashMap<String, Object>();

			File pdfFile = jasperFile.getParentFile();

			final JasperDesign jd = JRXmlLoader.load(jasperFile);

			final JasperReport jasperReport = JasperCompileManager.compileReport(jd);

			cantidadBiblioteca = bibliotecaService.contarBibliotecaByDate(start, end);
			cantidadRegistrador = registradorService.contarRegistradorByDate(start, end);
			cantidadConstancias = bibliotecaMasterService.contarConstanciasEmitidas(start, end);

			List<BibliotecaMaster> bibliotecaMasterListSeleccion = bibliotecaMasterService.listarConstanciasEmitidas(start, end);
			JRBeanCollectionDataSource  bibliotecaMasterList = new JRBeanCollectionDataSource(bibliotecaMasterListSeleccion);
			
			parameters.clear();
			parameters.put("cantidadRegistradores", cantidadRegistrador);
			parameters.put("cantidadBibibliotecas", cantidadBiblioteca);
			parameters.put("cantidadConstancias", cantidadConstancias);

			System.out.println(pdfFile.getAbsolutePath());
			// JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, bibliotecaMasterList);

			JasperExportManager.exportReportToPdfFile(jasperPrint,
					pdfFile.getAbsolutePath() + "/" + "reporte" + ".pdf");

			File enviarPdf = ResourceUtils.getFile(pdfFile.getAbsolutePath() + "/" + "reporte" + ".pdf");
			encodedString = Base64.getEncoder().encodeToString(Files.readAllBytes(enviarPdf.toPath()));
			
			documentoRespuesta.setNombreDocumento("La operación finalizaco sin inconvenientes ");
			documentoRespuesta.setDataDocumento(encodedString);
			documentoRespuesta.setNombreBiblioteca("reporte");

		} catch (JRException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return documentoRespuesta;
	}
	
	public static List<BibliotecaMaster> listar() {
		return null;
	}

}
