package pe.bnp.otie.rnb.service;

import pe.bnp.otie.rnb.dominio.Usuario;

public interface UsuarioService {

	Boolean verificarUsuarioFirma(Long idUsuario);
	
	Usuario buscarUsuario(Usuario usuario);
}
