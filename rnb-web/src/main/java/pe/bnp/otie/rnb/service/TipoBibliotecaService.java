package pe.bnp.otie.rnb.service;

import java.util.List;

import pe.bnp.otie.rnb.dominio.TipoBiblioteca;

public interface TipoBibliotecaService {
	
	List<TipoBiblioteca> listaTipoBiblioteca(Long idTipoBiblioteca);

}
