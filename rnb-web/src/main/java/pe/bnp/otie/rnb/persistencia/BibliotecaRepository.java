package pe.bnp.otie.rnb.persistencia;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.bnp.otie.rnb.dominio.Biblioteca;
import pe.bnp.otie.rnb.dominio.Registrador;
 
@Repository
public interface BibliotecaRepository extends JpaRepository<Biblioteca, Long>  {
	
	Biblioteca findByIdBiblioteca(Long idBiblioteca);
	
	List<Biblioteca> findAllByFechaInscripcionBetween(Date star, Date end);
	
}
