package pe.bnp.otie.rnb.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.bnp.otie.rnb.dominio.TablaMaestra;

public interface TablaMaestraRepository extends JpaRepository<TablaMaestra, Long> {
	
	TablaMaestra findByIdMaestra(Long idMaestra);  

}
