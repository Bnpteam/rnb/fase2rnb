package pe.bnp.otie.rnb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.ActualizarMigracion;
import pe.bnp.otie.rnb.dominio.Entidad;
import pe.bnp.otie.rnb.service.EntidadService;

@RestController
@RequestMapping("/entidad")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class EntidadController {

	@Autowired
	private EntidadService entidadService;

	@RequestMapping(value = "/actualizarRuc", method = RequestMethod.POST)
	public Entidad updateRucEntidad(@RequestBody ActualizarMigracion actualizarMigracion) {
		return entidadService.actualizarEntidad(actualizarMigracion.getRuc(), actualizarMigracion.getRucNuevo(),
				actualizarMigracion.getRazonSocial(), actualizarMigracion.getDireccion(), actualizarMigracion.getIdBiblioteca());
	}
}
