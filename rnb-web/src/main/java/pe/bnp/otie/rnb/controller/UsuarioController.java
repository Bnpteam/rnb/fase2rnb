package pe.bnp.otie.rnb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class UsuarioController {
	@Autowired
	private UsuarioService usuarioService;
	
	/** verifica si puede firmar */
	@RequestMapping(value = "/verificar", method = RequestMethod.GET)
	public Boolean verificarUsuario(@RequestParam(required = false) Long idUsuario ) { 
		return usuarioService.verificarUsuarioFirma(idUsuario);
	}
	
 
}
