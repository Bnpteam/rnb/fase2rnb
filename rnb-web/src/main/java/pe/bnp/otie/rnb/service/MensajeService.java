package pe.bnp.otie.rnb.service;
/**
 * Autor: Henrry Aris
 * Correo: ivan.hariasaqp@gmail.com
 * Fecha Creación: 14-11-2019
 */
import java.util.List;

import pe.bnp.otie.rnb.dominio.Mensaje; 

public interface MensajeService {
	Mensaje crearMensaje(Mensaje mensaje);
	
	List<Mensaje>  listMensaje();
	
	List<Mensaje> listarMensaje(Mensaje mensaje);
	
	Mensaje responderMensaje(Mensaje mensaje);
	
	Mensaje ocultarMensaje(Mensaje mensaje); 
	
	
	Long cantidadMensaje();
	
	Long cantidadRespuestas();
	
	Long cantidadBloqueado();
	
}
