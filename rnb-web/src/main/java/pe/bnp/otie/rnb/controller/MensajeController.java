package pe.bnp.otie.rnb.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.bnp.otie.rnb.dominio.Mensaje;
import pe.bnp.otie.rnb.service.MensajeService;

@RestController
@RequestMapping("/mensaje")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class MensajeController {
	@Autowired
	private MensajeService mensajeService;
	
	
	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public Mensaje crearMensaje(@RequestBody Mensaje mensaje) {
		mensaje.setFechaEnvio(new Date());
		mensaje.setFechaRegistro(new Date());
		return mensajeService.crearMensaje(mensaje);
	}
	
	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public List<Mensaje> listaMensaje() { 
		
		Mensaje mensaje=  new Mensaje();
		mensaje.setEstado("1");
		
		return mensajeService.listarMensaje(mensaje);
	}
	
	@RequestMapping(value = "/bloquearMensaje", method = RequestMethod.POST)
	public Mensaje bloquearMensaje(@RequestBody Mensaje mensaje) {
		return mensajeService.ocultarMensaje(mensaje);
	}
	
	@RequestMapping(value = "/reponderMensaje", method = RequestMethod.POST)
	public Mensaje  listaMensaje(@RequestBody Mensaje mensaje) { 		
		return mensajeService.responderMensaje(mensaje);
	} 
	
	@RequestMapping(value = "/contarRespuesta", method = RequestMethod.GET)
	public Long getContarRespuesta() {
		return mensajeService.cantidadRespuestas();
	}
	
	@RequestMapping(value = "/contarBloqueado", method = RequestMethod.GET)
	public Long getContarBloqueado() {
		return mensajeService.cantidadBloqueado();
	}
	
	@RequestMapping(value = "/contarMensaje", method = RequestMethod.GET)
	public Long getContarMensaje() {
		return mensajeService.cantidadMensaje();
	}
	
	 
}
