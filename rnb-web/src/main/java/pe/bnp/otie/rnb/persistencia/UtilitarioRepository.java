package pe.bnp.otie.rnb.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.bnp.otie.rnb.dominio.Utilitario;

public interface UtilitarioRepository extends JpaRepository<Utilitario, Long> {

	Utilitario findByIdUtilitario(Long idUtilitario);

}
