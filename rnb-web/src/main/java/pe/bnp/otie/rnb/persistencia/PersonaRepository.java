package pe.bnp.otie.rnb.persistencia;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.bnp.otie.rnb.dominio.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Long> {
	Persona findByIdPersona(Long idPersona);

}
