package pe.bnp.otie.rnb.service;

import java.util.Date;
import java.util.List;

import pe.bnp.otie.rnb.dominio.Registrador;

public interface RegistradorService {

	List<Registrador> listarRegistrador();

	List<Registrador> listarRegistradorByDate(Date star, Date end);

	Long contarRegistradorByDate(Date star, Date end);

	Boolean esRegistrador(Registrador registrador);

	Registrador buscarRegistrador(Registrador registrador);
}
