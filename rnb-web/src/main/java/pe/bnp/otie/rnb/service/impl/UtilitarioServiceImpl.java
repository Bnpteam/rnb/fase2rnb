package pe.bnp.otie.rnb.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.TablaMaestra;
import pe.bnp.otie.rnb.dominio.Utilitario;
import pe.bnp.otie.rnb.persistencia.TablaMaestraRepository;
import pe.bnp.otie.rnb.persistencia.UtilitarioRepository;
import pe.bnp.otie.rnb.service.TablaMaestraService;
import pe.bnp.otie.rnb.service.UtilitarioService;

@Service("UtilitarioService")
public class UtilitarioServiceImpl implements UtilitarioService {

	@Autowired
	UtilitarioRepository utilitarioRepository;

	@Autowired
	TablaMaestraService tablaMaestraService;

	@Autowired
	TablaMaestraRepository tablaMaestraRepository;

	@Override
	public Utilitario crearUtilitario(Utilitario utilitario) {

		TablaMaestra tablaMaestra = new TablaMaestra();

		tablaMaestra = tablaMaestraRepository.findByIdMaestra(utilitario.getTipoUtilitarioId());
		try {
			if (tablaMaestra.getEstado().equals("1")
					& utilitario.getFechaInicioPublicacion().before(utilitario.getFechaFinPublicacion())) {
				utilitario.setFechaRegistro(new Date());
				utilitario.setEstado("1");
				utilitario.setTipoUtilitarioIdDescripcion(tablaMaestra.getDescripcionLarga());

				return utilitarioRepository.save(utilitario);
			}
			utilitario.setIdUtilitario(new Long(-1));

		} catch (Exception e) {

			return utilitario;
		}

		return utilitario;
	}

	@Override
	public List<Utilitario> listaUtilitario(Utilitario utilitario) {
		Example<Utilitario> example = Example.of(utilitario);
		return utilitarioRepository.findAll(example);
	}

	@Override
	public List<Utilitario> listaUtilitario() {
		return utilitarioRepository.findAll();
	}

	@Override
	public Utilitario ocultarUtilitario(Utilitario utilitario) {
		utilitario.setEstado("0");
		utilitario.setFechaModificacion(new Date());

		return utilitarioRepository.save(utilitario);

	}

	@Override
	public Utilitario editarUtilitario(Utilitario utilitario) {

		TablaMaestra tablaMaestra = new TablaMaestra();

		tablaMaestra = tablaMaestraRepository.findByIdMaestra(utilitario.getTipoUtilitarioId());
		try {
			if (tablaMaestra.getEstado().equals("1")
					& utilitario.getFechaInicioPublicacion().before(utilitario.getFechaFinPublicacion())) {
				utilitario.setTipoUtilitarioIdDescripcion(tablaMaestra.getDescripcionLarga());
				utilitario.setFechaModificacion(new Date()); 
				utilitario.setEstado("1");

				return utilitarioRepository.save(utilitario);
			} else {
				utilitario.setIdUtilitario(new Long(-1));
				utilitario.setDescripcion("El tipo estado no esta activo");

			}

		} catch (Exception e) {

			return utilitario;
		}

		return utilitario;
	}

	@Override
	public Long cantidadUtilitario(Utilitario utilitario) {

		Long contador = new Long(listaUtilitario(utilitario).size());

		if (contador > 0) {
			return contador;
		}

		return new Long(0);
	}

	@Override
	public List<Utilitario> listarSegunTipo(Long idTipoUtilitario) {
		
		TablaMaestra tablaMaestra = new TablaMaestra();
		tablaMaestra = tablaMaestraRepository.findByIdMaestra(idTipoUtilitario);
		
		Utilitario utilitario = new Utilitario();
		
		if(tablaMaestra.getEstado().equals("1")) {
			utilitario.setTipoUtilitarioId(tablaMaestra.getIdMaestra());
			utilitario.setEstado("1");
		}
			
		return listaUtilitario(utilitario);

 
	}

}
