package pe.bnp.otie.rnb.service;

import pe.bnp.otie.rnb.dominio.EntidadBiblioteca;

public interface EntidadBibliotecaService {
	
	EntidadBiblioteca actualizar(EntidadBiblioteca entidadBiblioteca);
	EntidadBiblioteca buscarEntidad(EntidadBiblioteca entidadBiblioteca);
}
