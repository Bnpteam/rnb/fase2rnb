package pe.bnp.otie.rnb.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.Biblioteca;
import pe.bnp.otie.rnb.persistencia.BibliotecaRepository;
import pe.bnp.otie.rnb.service.BibliotecaService;

@Service("bibliotecaService")
public class BibliotecaServiceImpl implements BibliotecaService {

	@Autowired
	BibliotecaRepository bibliotecaRepository;

	@Override
	public Biblioteca buscarBibliotecaOrigen(Long idBiblioteca) {
		// TODO Auto-generated method stub
		Biblioteca biblioteca = new Biblioteca();
		biblioteca.setIdBiblioteca(idBiblioteca);

		Example<Biblioteca> example = Example.of(biblioteca);

		return bibliotecaRepository.findOne(example);
	}

	@Override
	public Biblioteca actualizarRegistrador(Long idBiblioteca, Long idRegistrador) {
		Biblioteca biblioteca = new Biblioteca();

		biblioteca = buscarBibliotecaOrigen(idBiblioteca);
		biblioteca.setIdRegistrador(idRegistrador);

		return bibliotecaRepository.save(biblioteca);
	}

	@Override
	public List<Biblioteca> listarBiblioteca(Biblioteca biblioteca) {
		Example<Biblioteca> example = Example.of(biblioteca);
		return bibliotecaRepository.findAll(example);
	}

	/**
	 * cantidad de bibliotecas pro estado en proceso por revalidar observado por
	 * verificar
	 */
	public Long cantidadSolicitudBiblioteca() {
		Long enProceso = (long) 0, porRevalidar = (long) 0, observado = (long) 0, porVerificar = (long) 0;

		Biblioteca bibliotecaEnProceso = new Biblioteca();
		bibliotecaEnProceso.setTipoEstadoBibliotecaId((long) 50);
		enProceso = (long) listarBiblioteca(bibliotecaEnProceso).size();

		Biblioteca bibliotecaPorRevalidar = new Biblioteca();
		bibliotecaPorRevalidar.setTipoEstadoBibliotecaId((long) 52);
		porRevalidar = (long) listarBiblioteca(bibliotecaPorRevalidar).size();

		Biblioteca bibliotecaObservado = new Biblioteca();
		bibliotecaObservado.setTipoEstadoBibliotecaId((long) 53);
		observado = (long) listarBiblioteca(bibliotecaObservado).size();

		Biblioteca bibliotecaPorVerificar = new Biblioteca();
		bibliotecaPorVerificar.setTipoEstadoBibliotecaId((long) 49);
		porVerificar = (long) listarBiblioteca(bibliotecaPorVerificar).size();

		return enProceso + porRevalidar + observado + porVerificar;
	}

	@Override
	public List<Biblioteca> listarBibliotecaByDate(Date star, Date end) {
		return bibliotecaRepository.findAllByFechaInscripcionBetween(star, end);
	}

	@Override
	public Long contarBibliotecaByDate(Date star, Date end) {
	 
		return new Long(bibliotecaRepository.findAllByFechaInscripcionBetween(star, end).size());
	}

	@Override
	public List<Biblioteca> buscarBibliotecaPorRegistrador(Long idRegistrador) {
		Biblioteca biblioteca = new Biblioteca();
		biblioteca.setIdRegistrador(idRegistrador);
		
		return listarBiblioteca(biblioteca);
	}

	@Override
	public Biblioteca buscarBiblioteca(Long idBiblioteca) {
		// TODO Auto-generated method stub
		return bibliotecaRepository.findByIdBiblioteca(idBiblioteca);
	}

}
