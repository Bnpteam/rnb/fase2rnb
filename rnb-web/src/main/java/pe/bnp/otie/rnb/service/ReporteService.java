package pe.bnp.otie.rnb.service;

import java.util.Date;

import pe.bnp.otie.rnb.dominio.DocumentoRespuesta;

public interface ReporteService {
	
	DocumentoRespuesta generarReportePorMes(Date star, Date end);
}
