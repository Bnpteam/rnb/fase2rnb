package pe.bnp.otie.rnb.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.BibliotecaMaster;
import pe.bnp.otie.rnb.dominio.Noticia;
import pe.bnp.otie.rnb.dominio.Responsable;
import pe.bnp.otie.rnb.dominio.TablaMaestra;
import pe.bnp.otie.rnb.dominio.TipoBiblioteca;
import pe.bnp.otie.rnb.persistencia.BibliotecaMasterRepository;
import pe.bnp.otie.rnb.persistencia.NoticiaRepository;
import pe.bnp.otie.rnb.persistencia.TablaMaestraRepository;
import pe.bnp.otie.rnb.persistencia.TipoBibliotecaRepository;
import pe.bnp.otie.rnb.service.NoticiaService;
import pe.bnp.otie.rnb.service.ResponsableService;

@Service("NoticiaService")
public class NoticiaServiceImpl implements NoticiaService {
	@Autowired
	NoticiaRepository noticiaRepository;

	@Autowired
	BibliotecaMasterRepository bibliotecaMasterRepository;

	@Autowired
	TablaMaestraRepository tablaMaestraRepository;

	@Autowired
	TipoBibliotecaRepository tipoBibliotecaRepository;

	@Autowired
	ResponsableService responsableService;
	
	@Override
	public Noticia crearNoticia(Noticia noticia) {

		TipoBiblioteca tipoBiblioteca = new TipoBiblioteca();
		TablaMaestra tipoEvento = new TablaMaestra();
		BibliotecaMaster bibliotecaMaster = new BibliotecaMaster();

		tipoBiblioteca = tipoBibliotecaRepository.findByIdTipoBiblioteca(noticia.getIdtipoBiblioteca());
		noticia.setTipoBiblioteca(tipoBiblioteca.getNombre());
		tipoBiblioteca = null;
		
		
		Responsable responsable = new Responsable();
		responsable.setIdBiblioteca(noticia.getIdBiblioteca());
		/** obtener el idPersona*/
		responsable = responsableService.buscarResponsable(responsable);
 
		noticia.setIdResponsable(responsable.getIdPersona());
		

		if(noticia.getIdSubTipoBiblioteca() != null && noticia.getIdSubTipoBiblioteca()>0) {
		tipoBiblioteca = tipoBibliotecaRepository.findByIdTipoBiblioteca(noticia.getIdSubTipoBiblioteca());
		noticia.setSubTipoBiblioteca(tipoBiblioteca.getNombre());
		tipoBiblioteca = null;
		}

		bibliotecaMaster = bibliotecaMasterRepository.findByBibliotecaMasterId(noticia.getIdBibliotecaMaster());
		noticia.setNombreBiblioteca(bibliotecaMaster.getNombre());

		tipoEvento = tablaMaestraRepository.findByIdMaestra(noticia.getTipoEventoId());
		noticia.setTipoEvento(tipoEvento.getDescripcionLarga());


		/**
		 * ESTADO 2: ESTADO INICIAL DE UN EVENTO
		 */
		noticia.setEstado("2");
		// noticia.setUsuarioIdRegistro(noticia.getIdResponsable());
		noticia.setFechaRegistro(new Date());

		return noticiaRepository.save(noticia);
	}

	@Override
	public List<Noticia> listaNoticias() {
		return noticiaRepository.findAll();
	}

	@Override
	public List<Noticia> listaNoticias(Noticia noticia) {

		Example<Noticia> example = Example.of(noticia);
		return noticiaRepository.findAll(example);
	}

	@Override
	public Noticia aprobarNoticia(Noticia noticia) {
		/** ESTADO 1: la noticia fue aprobada */
		if (noticia.getEstado().equals("2")) {
			noticia.setEstado("1");
			noticia.setFechaModificacion(new Date());
		}
		return noticiaRepository.save(noticia);
	}

	@Override
	public List<Noticia> listaNoticiaTipo(Noticia noticia) {

		Date fechaEvento = new Date();

		return listaNoticiaPorFecha(noticia.getTipoEventoId(), fechaEvento);
	}

	@Override
	public List<Noticia> listaNoticia(Long tipoEventoId, Long idBiblioteca) {

		Date fechaEvento = new Date();

		return noticiaRepository.getNoticiasPorBiblioteca(fechaEvento, tipoEventoId, idBiblioteca);
	}

	@Override
	public List<Noticia> listaNoticiaPorFecha(Long tipoEventoId, Date fechaEvento) {

		return noticiaRepository.getNoticias(fechaEvento, tipoEventoId);

	}
	 
	

	@Override
	public List<Noticia> listaNoticiaPorDepartamento(String departamento) {
		Date fechaEvento = new Date();
 		return noticiaRepository.getNoticiaPorDepartamento(fechaEvento, departamento.toUpperCase());
	}

	@Override
	public List<Noticia> listTipoBibliotecaNoticias(Long idtipoBiblioteca) {
		Date fechaEvento = new Date();
		
 		return noticiaRepository.getNoticiasTipoBiblioteca(fechaEvento, idtipoBiblioteca);
	}

}
