package pe.bnp.otie.rnb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.TablaMaestra;
import pe.bnp.otie.rnb.persistencia.TablaMaestraRepository;
import pe.bnp.otie.rnb.service.TablaMaestraService;

@Service("TablaMaestraService")
public class TablaMaestraServiceImpl implements TablaMaestraService {

	@Autowired
	TablaMaestraRepository tablaMaestraRepository;
	
	@Override
	public List<TablaMaestra> listarSubTabla(TablaMaestra tipoTabla) {
		Example<TablaMaestra> example = Example.of(tipoTabla);
		return tablaMaestraRepository.findAll(example);
	}
	

}
