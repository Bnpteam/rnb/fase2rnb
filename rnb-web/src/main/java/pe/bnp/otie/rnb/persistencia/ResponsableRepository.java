package pe.bnp.otie.rnb.persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.bnp.otie.rnb.dominio.Responsable;

public interface ResponsableRepository extends JpaRepository<Responsable, Long> {
	
	Responsable findByIdResponsable(Long idResponsable);
		
	@Query("select  r.idResponsable, r.idBiblioteca, r.idPersona, r.estado, r.correo from  Responsable r  where r.estado != :estado" )
	List<Responsable> findByResponsableCorreo(@Param("estado") String estado); 
	
}
