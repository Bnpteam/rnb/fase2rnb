package pe.bnp.otie.rnb.service.impl;

import java.util.Date;

/**
 * Autor: Henrry Aris
 * Correo: ivan.hariasaqp@gmail.com
 * Fecha Creación: 14-11-2019
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.bnp.otie.rnb.dominio.Mensaje;
import pe.bnp.otie.rnb.dominio.TablaMaestra;
import pe.bnp.otie.rnb.persistencia.MensajeRepository;
import pe.bnp.otie.rnb.persistencia.TablaMaestraRepository;
import pe.bnp.otie.rnb.service.MensajeService;
import pe.bnp.otie.rnb.service.TablaMaestraService;

@Service("MensajeService")
public class MensajeServiceImpl implements MensajeService {

	@Autowired
	MensajeRepository mensajeRepository;

	@Autowired
	TablaMaestraService tablaMaestraService;

	@Autowired
	TablaMaestraRepository tablaMaestraRepository;

	@Override
	public Mensaje crearMensaje(Mensaje mensaje) { 
		Long idTipoMensajeComprobar = mensaje.getIdTipoMensaje();

		TablaMaestra tablaMaestra = tablaMaestraRepository.findOne(idTipoMensajeComprobar);
 
		if (tablaMaestra.getTipoTabla().equals("TIPO_MENSAJE_ID") && tablaMaestra.getEstado().equals("1")) {
			Date fechaEnvio = new Date();
			Date fechaRegistro = new Date();
			mensaje.setFechaEnvio(fechaEnvio);
			mensaje.setFechaRegistro(fechaRegistro);
			mensaje.setIdTipoMensajeDetalle(tablaMaestra.getDescripcionLarga());
			mensaje.setUsuarioIdRegistro(new Long(99999));
			mensaje.setAsunto(tablaMaestra.getDescripcionLarga());
			mensaje.setEstado("1");			
			return mensajeRepository.save(mensaje);
		}

		mensaje.setIdMensaje(new Long(-1));
		mensaje.setDetalle("Debe seleccionar un tipo de mensaje correcto");
		return mensaje;
	}

	@Override
	public List<Mensaje> listMensaje() {

		return mensajeRepository.findAll();
	}

	@Override
	public List<Mensaje> listarMensaje(Mensaje mensaje) {
		Example<Mensaje> example = Example.of(mensaje);
		return mensajeRepository.findAll(example);
	}

	@Override
	public Mensaje responderMensaje(Mensaje mensaje) {
		
		if(mensaje.getEstado().equals("1")) {
			mensaje.setFechaModificacion(new Date());
			mensaje.setEstado("2");
			mensaje.setFechaRespuesta(new Date());
						
			return mensajeRepository.save(mensaje);
		} else {
			mensaje.setIdMensaje(new Long(-1));
		} 

		return mensaje;
	}

	@Override
	public Mensaje ocultarMensaje(Mensaje mensaje) {

		if(mensaje.getEstado().equals("1")) {
			mensaje.setFechaModificacion(new Date());
			mensaje.setEstado("0");
						
			return mensajeRepository.save(mensaje);
		} else {
			mensaje.setIdMensaje(new Long(-1));
		}

		return mensaje;
		
	}

	@Override
	public Long cantidadMensaje() {
   		return new Long(mensajeRepository.findAll().size());
	}

	@Override
	public Long cantidadRespuestas() {
		Mensaje mensaje = new Mensaje();
		mensaje.setEstado("2");
		return new Long(listarMensaje(mensaje).size());
	}

	@Override
	public Long cantidadBloqueado() {
		Mensaje mensaje = new Mensaje();
		mensaje.setEstado("0");
 		return new Long(listarMensaje(mensaje).size());
	}

}
