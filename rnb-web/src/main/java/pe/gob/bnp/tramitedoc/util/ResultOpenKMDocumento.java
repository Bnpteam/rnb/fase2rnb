package pe.gob.bnp.tramitedoc.util;

public class ResultOpenKMDocumento {

	private String uiid;
	private String codigo;
	private String descripcion;
	
	private byte[] rawFile;

	public ResultOpenKMDocumento(){
		this.setCodigo("");
		this.setUiid("");
		this.setDescripcion("");
	}
	

	public String getUiid() {
		return uiid;
	}

	public void setUiid(String uiid) {
		this.uiid = uiid;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public byte[] getRawFile() {
		return rawFile;
	}

	public void setRawFile(byte[] rawFile) {
		this.rawFile = rawFile;
	}	
	
}
