package pe.gob.bnp.tramitedoc.util;

import java.io.Serializable;
import java.util.Date;

public class FileOpenKM  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String		fileName;
	private String 		fileNameFechaHoraMinutos;
	private String		extension;
	private byte[]		rawFile;
	
	private String		model;
	private String		tipoDoc;
	
	private String		uiid;
	
	private String		numDocumento;
	private Date		fechaDocumento;
	private String		rutaServidorTemp;
	
	private boolean  	principal;
	private String		numeroEmision;
	private String 		fechaUpload;
	private String 		numeroExpediente;
	private String		dependencia;
		
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileNameFechaHoraMinutos() {
		return fileNameFechaHoraMinutos;
	}
	public void setFileNameFechaHoraMinutos(String fileNameFechaHoraMinutos) {
		this.fileNameFechaHoraMinutos = fileNameFechaHoraMinutos;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public byte[] getRawFile() {
		return rawFile;
	}
	public void setRawFile(byte[] rawFile) {
		this.rawFile = rawFile;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getUiid() {
		return uiid;
	}
	public void setUiid(String uiid) {
		this.uiid = uiid;
	}
	public String getNumDocumento() {
		return numDocumento;
	}
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}
	public Date getFechaDocumento() {
		return fechaDocumento;
	}
	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	public String getRutaServidorTemp() {
		return rutaServidorTemp;
	}
	public void setRutaServidorTemp(String rutaServidorTemp) {
		this.rutaServidorTemp = rutaServidorTemp;
	}
	public boolean isPrincipal() {
		return principal;
	}
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	public String getNumeroEmision() {
		return numeroEmision;
	}
	public void setNumeroEmision(String numeroEmision) {
		this.numeroEmision = numeroEmision;
	}
	public String getFechaUpload() {
		return fechaUpload;
	}
	public void setFechaUpload(String fechaUpload) {
		this.fechaUpload = fechaUpload;
	}
	public String getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	public String getDependencia() {
		return dependencia;
	}
	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}
	
	public String getAnnioFechaDocumento(){
		return DateUtil.getAnioFechaExt(this.getFechaDocumento());
	}
	public String getMesFechaDocumento(){
		return DateUtil.getMesFechaExt(this.getFechaDocumento());
	}
	public String getDiaFechaDocumento(){
		return DateUtil.getDiaFechaExt(this.getFechaDocumento());
	}

}
