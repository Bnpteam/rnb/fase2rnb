package pe.bnp.otie.rnb.proxy;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import pe.bnp.otie.rnb.dominio.BibliotecaMaster; 
 
public class BibliotecaMasterREST {
	
	protected static final Logger LOGGER = LogManager.getLogger(BibliotecaMasterREST.class);

	protected final RestTemplate restTemplate;

	protected String url;
	
	public BibliotecaMasterREST(RestTemplate restTemplate, String url) {
		super();
		this.restTemplate = restTemplate;
		this.url = url;
	}
	
	public List<BibliotecaMaster> listar() {
		UriComponents uriComponents;
		ResponseEntity<BibliotecaMaster[]> responseEntity;

		uriComponents = UriComponentsBuilder.fromHttpUrl(url).pathSegment("bibliotecaMaster", "consultar").build();

		responseEntity = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, null, BibliotecaMaster[].class);

		if (responseEntity.getStatusCode().is2xxSuccessful()) {
			return Arrays.asList(responseEntity.getBody());
		}

		return null;

	}
}
