package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_UTILITARIOS", schema = "RNB")
public class Utilitario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6306021078703702474L;

	private Long idUtilitario;
	private Long tipoUtilitarioId;
	private String detalle; 
	private String estado;
	private String descripcion;
	private Date fechaRegistro;
	private Date fechaModificacion;
	private Long usuarioIdRegistro;
	private Long usuarioIdModificacion;
	private Date fechaInicioPublicacion;
	private Date fechaFinPublicacion;
	private String tipoUtilitarioIdDescripcion;

	@Id
	@Column(name = "ID_UTILITARIO")
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getIdUtilitario() {
		return idUtilitario;
	}

	public void setIdUtilitario(Long idUtilitario) {
		this.idUtilitario = idUtilitario;
	}

	@Column(name = "TIPO_UTILITARIO_ID")
	public Long getTipoUtilitarioId() {
		return tipoUtilitarioId;
	}

	public void setTipoUtilitarioId(Long tipoUtilitarioId) {
		this.tipoUtilitarioId = tipoUtilitarioId;
	}

	@Column
	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	} 
	
	@Column
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name = "FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "USUARIO_ID_REGISTRO")
	public Long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}

	public void setUsuarioIdRegistro(Long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}

	@Column(name = "USUARIO_ID_MODIFICACION")
	public Long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}

	public void setUsuarioIdModificacion(Long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}

	@Column
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "FECHA_INICIO_PUBLICACION")
	public Date getFechaInicioPublicacion() {
		return fechaInicioPublicacion;
	}

	public void setFechaInicioPublicacion(Date fechaInicioPublicacion) {
		this.fechaInicioPublicacion = fechaInicioPublicacion;
	}

	@Column(name="FECHA_FIN_PUBLICACION")
	public Date getFechaFinPublicacion() {
		return fechaFinPublicacion;
	}

	public void setFechaFinPublicacion(Date fechaFinPublicacion) {
		this.fechaFinPublicacion = fechaFinPublicacion;
	}

	@Column(name = "TIPO_UTILITARIO_ID_DESCRIPCION")
	public String getTipoUtilitarioIdDescripcion() {
		return tipoUtilitarioIdDescripcion;
	}

	public void setTipoUtilitarioIdDescripcion(String tipoUtilitarioIdDescripcion) {
		this.tipoUtilitarioIdDescripcion = tipoUtilitarioIdDescripcion;
	}

	
	
}
