package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;

public class RespuestaServicio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1815520124265691593L;

	private String codigo;
	private String detalle;
	private String errores;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getErrores() {
		return errores;
	}

	public void setErrores(String errores) {
		this.errores = errores;
	}

}
