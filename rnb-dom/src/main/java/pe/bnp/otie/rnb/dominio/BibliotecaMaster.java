package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_BIBLIOTECA_MASTER", schema = "RNB")
public class BibliotecaMaster implements Serializable {

	/**
	 * Bibliotecas qque fueron verificadas. existe un registro unico por cada biblioteca
	 */
	private static final long serialVersionUID = -10006541094697561L;
	
	private Long bibliotecaMasterId;
	private String nombre;
	private String numeroRnb;
	private Integer secuencia;
	private Long bibliotecaIdOrig;
	private String codDepartamento;
	private String departamento;
	private String codProvincia;
	private String provincia;
	private String codDistrito;
	private String distrito;
	private String ultAnioVerificado;
	private String column1; /** columna   flag 0: no firmado, 1: firmado 2: otro*/
	private String column2; /** ubicacion de registro */
	private String usuarioIdRegistro;
	private String fechaRegistro;
	private String usuarioIdModificacion;
	private Date fechaModificacion;
	
	@Id
	@Column(name = "BIBLIOTECA_MASTER_ID")
	public Long getBibliotecaMasterId() {
		return bibliotecaMasterId;
	}
	public void setBibliotecaMasterId(Long bibliotecaMasterId) {
		this.bibliotecaMasterId = bibliotecaMasterId;
	}

	@Column
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Column(name = "NUMERO_RNB")
	public String getNumeroRnb() {
		return numeroRnb;
	}
	public void setNumeroRnb(String numeroRnb) {
		this.numeroRnb = numeroRnb;
	}
	@Column
	public Integer getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}
	@Column(name="BIBLIOTECA_ID_ORIG")
	public Long getBibliotecaIdOrig() {
		return bibliotecaIdOrig;
	}
	public void setBibliotecaIdOrig(Long bibliotecaIdOrig) {
		this.bibliotecaIdOrig = bibliotecaIdOrig;
	}
	@Column
	public String getCodDepartamento() {
		return codDepartamento;
	}
	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}
	@Column
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	@Column
	public String getCodProvincia() {
		return codProvincia;
	}
	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}
	@Column
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	@Column
	public String getCodDistrito() {
		return codDistrito;
	}
	public void setCodDistrito(String codDistrito) {
		this.codDistrito = codDistrito;
	}
	@Column
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	@Column(name = "ULT_ANIO_VERIFICADO")
	public String getUltAnioVerificado() {
		return ultAnioVerificado;
	}
	public void setUltAnioVerificado(String ultAnioVerificado) {
		this.ultAnioVerificado = ultAnioVerificado;
	}
	@Column
	public String getColumn1() {
		return column1;
	}
	public void setColumn1(String column1) {
		this.column1 = column1;
	}
	@Column
	public String getColumn2() {
		return column2;
	}
	public void setColumn2(String column2) {
		this.column2 = column2;
	}
	@Column(name = "USUARIO_ID_REGISTRO")
	public String getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}
	public void setUsuarioIdRegistro(String usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}
	@Column(name = "FECHA_REGISTRO")
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	@Column(name = "USUARIO_ID_MODIFICACION")
	public String getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}
	public void setUsuarioIdModificacion(String usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
 
}
