package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_SUGERENCIA", schema = "RNB")
public class Sugerencia implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8963425098627371009L;
	
	private Long idSugerencia;
	private Long tipoSugerencaId;
	private String tipoSugerenciaDetalle;
	private String detalle;
	private Date fechaCarga;
	private Date fechaRegistro;
	private Long usuarioIdRegistro;
	private Long idPersona;
	private Date fechaModificacion;
	private Long usuarioIdModificacion;
	private String respuesta;
	private String estado;
	
	@Id
	@Column(name = "ID_SUGERENCIA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getIdSugerencia() {
		return idSugerencia;
	}
	public void setIdSugerencia(Long idSugerencia) {
		this.idSugerencia = idSugerencia;
	}
	
	@Column(name = "TIPO_SUGERENCIA_ID")
	public Long getTipoSugerencaId() {
		return tipoSugerencaId;
	}
	public void setTipoSugerencaId(Long tipoSugerencaId) {
		this.tipoSugerencaId = tipoSugerencaId;
	}
	
	@Column(name = "TIPO_SUGERENCIA_DETALLE")
	public String getTipoSugerenciaDetalle() {
		return tipoSugerenciaDetalle;
	}
	public void setTipoSugerenciaDetalle(String tipoSugerenciaDetalle) {
		this.tipoSugerenciaDetalle = tipoSugerenciaDetalle;
	}
	
	@Column 
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	@Column(name = "FECHA_CARGA")
	public Date getFechaCarga() {
		return fechaCarga;
	}
	
	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	
	@Column(name = "FECHA_REGISTRO" )
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	@Column(name = "USUARIO_ID_REGISTRO")
	public Long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}
	public void setUsuarioIdRegistro(Long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}
	
	@Column(name="PERSONA_ID")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	
	@Column(name="FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	@Column(name = "USUARIO_ID_MODIFICACION")
	public Long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}
	public void setUsuarioIdModificacion(Long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}
	
	@Column
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	@Column
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
