package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;

public class ActualizarMigracion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3019801823253874888L;

	private String ruc;
	private String rucNuevo;
	private String razonSocial;
	private String direccion;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String numeroDocumento;
	private Long idBiblioteca;
	
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getRucNuevo() {
		return rucNuevo;
	}
	public void setRucNuevo(String rucNuevo) {
		this.rucNuevo = rucNuevo;
	}

	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public Long getIdBiblioteca() {
		return idBiblioteca;
	}
	public void setIdBiblioteca(Long idBiblioteca) {
		this.idBiblioteca = idBiblioteca;
	}
	
	
	
	
}

