package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_PERSONA", schema = "RNB")
public class Persona implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -205703383822206923L;
	private Long idPersona;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String numerDocumento;
		
	@Id
	@Column(name = "PERSONA_ID")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	@Column
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	@Column(name = "APELLIDO_PATERNO")
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	
	@Column(name = "APELLIDO_MATERNO")
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	@Column(name = "NUMERO_DOCUMENTO")
	public String getNumerDocumento() {
		return numerDocumento;
	}
	public void setNumerDocumento(String numerDocumento) {
		this.numerDocumento = numerDocumento;
	}
	
	
}
