package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_BIBLIOTECA", schema = "RNB")
public class Biblioteca implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6098092232872713881L;

	private Long idBiblioteca;
	private String numeroRNB;
	private String AnioDeclaracion;
	private String nombre;
	private String departamento;
	private String provincia;
	private String distrito;
	private String direccion;
	private Long idRegistrador;
	private String tipoBiblioteca;
	private Long tipoBibliotecaId;
	private String subTipoBiblioteca;
	private Long subTipoBibliotecaId;
	private String tipoAmbito;
	private Long tipoAmbitoId;
	private String tipoFuncionamiento;
	private Long tipoFuncionamientoId;
	private String correo;
	private String tipoEstadoBiblioteca;
	private Long tipoEstadoBibliotecaId;
	private Date fechaRegistro;
	private Date fechaInscripcion;
	
	@Id
	@Column(name = "BIBLIOTECA_ID")
	public Long getIdBiblioteca() {
		return idBiblioteca;
	}

	public void setIdBiblioteca(Long idBiblioteca) {
		this.idBiblioteca = idBiblioteca;
	}

	@Column(name = "NUMERO_RNB")
	public String getNumeroRNB() {
		return numeroRNB;
	}

	public void setNumeroRNB(String numeroRNB) {
		this.numeroRNB = numeroRNB;
	}

	@Column(name = "ANIO_DECLARACION")
	public String getAnioDeclaracion() {
		return AnioDeclaracion;
	}

	public void setAnioDeclaracion(String anioDeclaracion) {
		AnioDeclaracion = anioDeclaracion;
	}

	@Column
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column
	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	@Column
	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	@Column
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@Column
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	@Column(name="REGISTRADOR_ID")
	public Long getIdRegistrador() {
		return idRegistrador;
	}

	public void setIdRegistrador(Long idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	@Column(name = "TIPO_BIBLIOTECA")
	public String getTipoBiblioteca() {
		return tipoBiblioteca;
	}

	public void setTipoBiblioteca(String tipoBiblioteca) {
		this.tipoBiblioteca = tipoBiblioteca;
	}
	
	@Column(name = "TIPO_BIBLIOTECA_ID")
	public Long getTipoBibliotecaId() {
		return tipoBibliotecaId;
	}

	public void setTipoBibliotecaId(Long tipoBibliotecaId) {
		this.tipoBibliotecaId = tipoBibliotecaId;
	}

	@Column
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Column(name = "TIPO_ESTADO_BIBLIOTECA")
	public String getTipoEstadoBiblioteca() {
		return tipoEstadoBiblioteca;
	}

	public void setTipoEstadoBiblioteca(String tipoEstadoBiblioteca) {
		this.tipoEstadoBiblioteca = tipoEstadoBiblioteca;
	}

	@Column(name = "TIPO_ESTADO_BIBLIOTECA_ID")
	public Long getTipoEstadoBibliotecaId() {
		return tipoEstadoBibliotecaId;
	}

	public void setTipoEstadoBibliotecaId(Long tipoEstadoBibliotecaId) {
		this.tipoEstadoBibliotecaId = tipoEstadoBibliotecaId;
	}

	@Column(name = "FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	@Column(name = "FECHA_INSCRIPCION")
	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	@Column(name = "SUB_TIPO_BIBLIOTECA")
	public String getSubTipoBiblioteca() {
		return subTipoBiblioteca;
	}

	public void setSubTipoBiblioteca(String subTipoBiblioteca) {
		this.subTipoBiblioteca = subTipoBiblioteca;
	}
	@Column(name = "SUB_TIPO_BIBLIOTECA_ID")
	public Long getSubTipoBibliotecaId() {
		return subTipoBibliotecaId;
	}

	public void setSubTipoBibliotecaId(Long subTipoBibliotecaId) {
		this.subTipoBibliotecaId = subTipoBibliotecaId;
	}
	@Column(name = "TIPO_AMBITO")
	public String getTipoAmbito() {
		return tipoAmbito;
	}

	public void setTipoAmbito(String tipoAmbito) {
		this.tipoAmbito = tipoAmbito;
	}
	@Column(name = "TIPO_AMBITO_ID")
	public Long getTipoAmbitoId() {
		return tipoAmbitoId;
	}

	public void setTipoAmbitoId(Long tipoAmbitoId) {
		this.tipoAmbitoId = tipoAmbitoId;
	}
	@Column(name = "TIPO_FUNCIONAMIENTO")
	public String getTipoFuncionamiento() {
		return tipoFuncionamiento;
	}

	public void setTipoFuncionamiento(String tipoFuncionamiento) {
		this.tipoFuncionamiento = tipoFuncionamiento;
	}
	@Column(name = "TIPO_FUNCIONAMIENTO_ID")
	public Long getTipoFuncionamientoId() {
		return tipoFuncionamientoId;
	}

	public void setTipoFuncionamientoId(Long tipoFuncionamientoId) {
		this.tipoFuncionamientoId = tipoFuncionamientoId;
	}
 
	
	
	
}
