package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_TIPO_BIBLIOTECA", schema = "RNB")
public class TipoBiblioteca implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1820882033050443309L;

	private Long idTipoBiblioteca;
	private String nombre;
	private Long tipoBibliotecaPadreId;
	private String estado;
	private Long usuarioIdRegistro;
	private Date fechaRegistro;
	private Long usuarioIdModificacion;
	private Date fechaModificacion;

	@Id
	@Column(name = "TIPO_BIBLIOTECA_ID")
	public Long getIdTipoBiblioteca() {
		return idTipoBiblioteca;
	}
	public void setIdTipoBiblioteca(Long idTipoBiblioteca) {
		this.idTipoBiblioteca = idTipoBiblioteca;
	}

	@Column
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "TIPO_BIBLIOTECA_PADRE_ID")
	public Long getTipoBibliotecaPadreId() {
		return tipoBibliotecaPadreId;
	}
	public void setTipoBibliotecaPadreId(Long tipoBibliotecaPadreId) {
		this.tipoBibliotecaPadreId = tipoBibliotecaPadreId;
	}

	@Column
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name = "USUARIO_ID_REGISTRO")
	public Long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}
	public void setUsuarioIdRegistro(Long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}

	@Column(name = "FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	@Column(name = "USUARIO_ID_MODIFICACION")
	public Long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}
	public void setUsuarioIdModificacion(Long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}

	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}
