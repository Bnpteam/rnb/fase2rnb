package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_MAESTRA", schema = "RNB")
public class TablaMaestra implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idMaestra;
	private String tipoTabla;
	private String DescripcionCorta;
	private String descripcionLarga;
	private Long usuarioIdRegistro;
	private Date fechaRegistro;
	private Long usuarioIdModificacion;
	private String estado;
	
	@Id
	@Column(name = "MAESTRA_ID")
	public Long getIdMaestra() {
		return idMaestra;
	}
	public void setIdMaestra(Long idMaestra) {
		this.idMaestra = idMaestra;
	}
	@Column(name = "TIPO_TABLA")
	public String getTipoTabla() {
		return tipoTabla;
	}
	public void setTipoTabla(String tipoTabla) {
		this.tipoTabla = tipoTabla;
	}
	
	@Column(name = "DESCRIPCION_CORTA")
	public String getDescripcionCorta() {
		return DescripcionCorta;
	}
	public void setDescripcionCorta(String descripcionCorta) {
		DescripcionCorta = descripcionCorta;
	}
	
	@Column(name = "DESCRIPCION_LARGA")
	public String getDescripcionLarga() {
		return descripcionLarga;
	}
	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}
	
	@Column(name = "USUARIO_ID_REGISTRO")
	public Long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}
	public void setUsuarioIdRegistro(Long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}
	
	@Column(name = "FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	@Column(name = "USUARIO_ID_MODIFICACION")
	public Long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}
	public void setUsuarioIdModificacion(Long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}
	
	@Column
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
	
}
