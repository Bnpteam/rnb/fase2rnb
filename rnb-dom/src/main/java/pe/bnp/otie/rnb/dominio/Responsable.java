package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_RESPONSABLE", schema = "RNB")
public class Responsable implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7932285472264157057L;
	
	private Long idBiblioteca;
	private Long idResponsable;
	private Long idPersona;
	private String estado;
	private String correo;	
	
	@Id
	@Column(name="RESPONSABLE_ID") 
	public Long getIdResponsable() {
		return idResponsable;
	}
	public void setIdResponsable(Long idResponsable) {
		this.idResponsable = idResponsable;
	}
	
	@Column(name="BIBLIOTECA_ID")
	public Long getIdBiblioteca() {
		return idBiblioteca;
	}
	public void setIdBiblioteca(Long idBiblioteca) {
		this.idBiblioteca = idBiblioteca;
	}
	
	@Column(name="PERSONA_ID")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	@Column
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	@Column
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	
	
}
