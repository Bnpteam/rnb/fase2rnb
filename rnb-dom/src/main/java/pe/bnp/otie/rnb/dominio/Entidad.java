package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_ENTIDAD", schema = "RNB")
public class Entidad implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1289016843035109083L;
	private Long idEntidad;
	private String razonSocial;
	private String ruc;
	private String flagOrigen;
	private String direccion;
	
	
	@Id
	@Column(name="ENTIDAD_ID")
	public Long getIdEntidad() {
		return idEntidad;
	}
	public void setIdEntidad(Long idEntidad) {
		this.idEntidad = idEntidad;
	}
	
	@Column(name="RAZON_SOCIAL")
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	
	@Column
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	
	@Column(name="FLAG_ORIGEN")
	public String getFlagOrigen() {
		return flagOrigen;
	}
	public void setFlagOrigen(String flagOrigen) {
		this.flagOrigen = flagOrigen;
	}
	
 

}
