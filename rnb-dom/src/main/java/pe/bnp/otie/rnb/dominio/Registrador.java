package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_REGISTRADOR", schema = "RNB")
public class Registrador implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -669563247508552112L;
	
	private Long idRegistrador;
	private Long idPersona;
	private String usuario;
	private Date fechaRegistro;
	private String estado;
	private String telefonoMovil;
	private String telefonoFijo;
	private String correo;
	
	@Id
	@Column(name = "REGISTRADOR_ID")
	public Long getIdRegistrador() {
		return idRegistrador;
	}
	public void setIdRegistrador(Long idRegistrador) {
		this.idRegistrador = idRegistrador;
	}
	
	@Column(name = "PERSONA_ID")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	
	@Column
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Column(name = "FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	@Column
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	@Column(name = "TELEFONO_MOVIL")
	public String getTelefonoMovil() {
		return telefonoMovil;
	}
	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}
	
	@Column(name = "TELEFONO_FIJO")
	public String getTelefonoFijo() {
		return telefonoFijo;
	}
	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}
	
	@Column
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	
	
}
