package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TBL_NOTICIA", schema = "RNB")
public class Noticia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9175528103717965575L;
	
	private Long idNoticia;
	private Long idResponsable;
	private String detalle;
	private String horaInicio;
	private String horaFin;
	private Date fechaInicio;
	private Date fechaFin;
	private String costo;
	private String cantidadMaximo;
	private String correEvento;
	private String telefono;
 	private String direccion;
	private String referencia;
	private String inscripcion;
	private Date fechaInicioInscripcion;
	private Date fechaFinInscripcion;
	private Date fechaRegistro;
	private Long usuarioIdRegistro;
	private Long usuarioIdModificacion;
	private Date fechaModificacion;
	private String estado;
	private Long tipoEventoId;	
	private String tipoEvento;
	private String paginaWeb;
	private Long idtipoBiblioteca;
	private Long idBiblioteca;
	private String nombreBiblioteca;
	private String tipoBiblioteca;
	private String subTipoBiblioteca;
	private Long idSubTipoBiblioteca;
	private String departamento;
	private String provincia;
	private String distrito;
	private String coddepartamento;
	private String codprovincia;
	private String coddistrito;
	private Long idBibliotecaMaster;
	
	
	
	@Transient
	public Long getIdBibliotecaMaster() {
		return idBibliotecaMaster;
	}
	public void setIdBibliotecaMaster(Long idBibliotecaMaster) {
		this.idBibliotecaMaster = idBibliotecaMaster;
	}
	
	@Id
	@Column(name = "ID_NOTICIA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getIdNoticia() {
		return idNoticia;
	}
	public void setIdNoticia(Long idNoticia) {
		this.idNoticia = idNoticia;
	}
	
	@Column(name = "RESPONSABLE_ID")
	public Long getIdResponsable() {
		return idResponsable;
	}
	public void setIdResponsable(Long idResponsable) {
		this.idResponsable = idResponsable;
	}
	
	@Column
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	@Column(name = "HORA_INICIO")
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	@Column(name = "HORA_FIN")
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	
	@Column(name = "FECHA_INICIO")
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	@Column(name = "FECHA_FIN")
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	@Column
	public String getCosto() {
		return costo;
	}
	public void setCosto(String costo) {
		this.costo = costo;
	}
	
	@Column(name = "CANTIDAD_MAXIMO")
	public String getCantidadMaximo() {
		return cantidadMaximo;
	}
	public void setCantidadMaximo(String cantidadMaximo) {
		this.cantidadMaximo = cantidadMaximo;
	}
	
	@Column(name = "CORREO_EVENTO")
	public String getCorreEvento() {
		return correEvento;
	}
	public void setCorreEvento(String correEvento) {
		this.correEvento = correEvento;
	}
	
	@Column
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	} 
	
	@Column
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	@Column
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	@Column
	public String getInscripcion() {
		return inscripcion;
	}
	public void setInscripcion(String inscripcion) {
		this.inscripcion = inscripcion;
	}
	
	@Column(name = "FECHA_INICIO_INSCRIPCION")
	public Date getFechaInicioInscripcion() {
		return fechaInicioInscripcion;
	}
	public void setFechaInicioInscripcion(Date fechaInicioInscripcion) {
		this.fechaInicioInscripcion = fechaInicioInscripcion;
	}
	
	@Column(name = "FECHA_FIN_INSCRIPCION")
	public Date getFechaFinInscripcion() {
		return fechaFinInscripcion;
	}
	public void setFechaFinInscripcion(Date fechaFinInscripcion) {
		this.fechaFinInscripcion = fechaFinInscripcion;
	}
	
	@Column(name="FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	@Column(name = "USUARIO_ID_REGISTRO")
	public Long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}
	public void setUsuarioIdRegistro(Long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}
	
	@Column(name = "USUARIO_ID_MODIFICACION")
	public Long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}
	public void setUsuarioIdModificacion(Long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}
	
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	@Column
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	@Column(name = "TIPO_EVENTO_ID")
	public Long getTipoEventoId() {
		return tipoEventoId;
	}
	public void setTipoEventoId(Long tipoEventoId) {
		this.tipoEventoId = tipoEventoId;
	}
	
	@Column(name = "PAGINA_WEB")
	public String getPaginaWeb() {
		return paginaWeb;
	}
	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}
	
	@Column(name = "TIPO_BIBLIOTECA_ID")
	public Long getIdtipoBiblioteca() {
		return idtipoBiblioteca;
	}
	public void setIdtipoBiblioteca(Long idtipoBiblioteca) {
		this.idtipoBiblioteca = idtipoBiblioteca;
	}
	
	@Column(name = "BIBLIOTECA_ID")
	public Long getIdBiblioteca() {
		return idBiblioteca;
	}
	public void setIdBiblioteca(Long idBiblioteca) {
		this.idBiblioteca = idBiblioteca;
	}
	
	@Column(name="NOMBRE_BIBLIOTECA")
	public String getNombreBiblioteca() {
		return nombreBiblioteca;
	}
	public void setNombreBiblioteca(String nombreBiblioteca) {
		this.nombreBiblioteca = nombreBiblioteca;
	}
	@Column(name = "SUB_TIPO_BIBLIOTECA_ID")
	public Long getIdSubTipoBiblioteca() {
		return idSubTipoBiblioteca;
	}
	public void setIdSubTipoBiblioteca(Long idSubTipoBiblioteca) {
		this.idSubTipoBiblioteca = idSubTipoBiblioteca;
	}
	
	@Column(name="TIPO_EVENTO")
	public String getTipoEvento() {
		return tipoEvento;
	}
	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	
	@Column
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	@Column
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	@Column
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	
	@Column
	public String getCoddepartamento() {
		return coddepartamento;
	}
	public void setCoddepartamento(String coddepartamento) {
		this.coddepartamento = coddepartamento;
	}
	
	@Column
	public String getCodprovincia() {
		return codprovincia;
	}
	public void setCodprovincia(String codprovincia) {
		this.codprovincia = codprovincia;
	}
	
	@Column
	public String getCoddistrito() {
		return coddistrito;
	}
	public void setCoddistrito(String coddistrito) {
		this.coddistrito = coddistrito;
	}
	
	@Column(name="TIPO_BIBLIOTECA")
	public String getTipoBiblioteca() {
		return tipoBiblioteca;
	}
	public void setTipoBiblioteca(String tipoBiblioteca) {
		this.tipoBiblioteca = tipoBiblioteca;
	}
	
	@Column(name="SUB_TIPO_BIBLIOTECA")
	public String getSubTipoBiblioteca() {
		return subTipoBiblioteca;
	}
	public void setSubTipoBiblioteca(String subTipoBiblioteca) {
		this.subTipoBiblioteca = subTipoBiblioteca;
	}
	

}
