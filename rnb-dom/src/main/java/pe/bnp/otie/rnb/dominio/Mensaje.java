package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_MENSAJE", schema = "RNB")
public class Mensaje implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 849039433340717844L;
	
	private Long idMensaje;
	private String detalle;
	private String asunto;
	private Date fechaEnvio;
	private Date fechaRespuesta;
	private String correoRemitente;
	private String telefono;
	private Date fechaRegistro;
	private Long usuarioIdRegistro;
	private Date fechaModificacion;
	private Long usuarioIdModificacion;
	private Long IdTipoMensaje;
	private String IdTipoMensajeDetalle;
	private String estado;
	private String respuesta;
	private String remitente;
		
	@Id
	@Column(name = "ID_MENSAJE")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getIdMensaje() {
		return idMensaje;
	}
	public void setIdMensaje(Long idMensaje) {
		this.idMensaje = idMensaje;
	}
	@Column
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	@Column(name = "FECHA_ENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	@Column(name = "FECHA_RESPUESTA")
	public Date getFechaRespuesta() {
		return fechaRespuesta;
	}
	public void setFechaRespuesta(Date fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}
	
	@Column(name="CORREO_REMITENTE")
	public String getCorreoRemitente() {
		return correoRemitente;
	}
	public void setCorreoRemitente(String correoRemitente) {
		this.correoRemitente = correoRemitente;
	}
	@Column
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Column(name = "FECHA_REGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	@Column(name = "USUARIO_ID_REGISTRO")
	public Long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}
	public void setUsuarioIdRegistro(Long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}
	@Column(name = "FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	@Column(name = "USUARIO_ID_MODIFICACION")
	public Long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}
	public void setUsuarioIdModificacion(Long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}
	@Column(name = "ID_TIPO_MENSAJE" )
	public Long getIdTipoMensaje() {
		return IdTipoMensaje;
	}
	public void setIdTipoMensaje(Long idTipoMensaje) {
		IdTipoMensaje = idTipoMensaje;
	}
	@Column(name = "ID_TIPO_MENSAJE_DETALLE")
	public String getIdTipoMensajeDetalle() {
		return IdTipoMensajeDetalle;
	}
	public void setIdTipoMensajeDetalle(String idTipoMensajeDetalle) {
		IdTipoMensajeDetalle = idTipoMensajeDetalle;
	}
	
	@Column
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	@Column
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	@Column
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	
	
	
}
