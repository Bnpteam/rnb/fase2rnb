package pe.bnp.otie.rnb.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_ENTIDAD_BIBLIOTECA", schema = "RNB")
public class EntidadBiblioteca implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7943547265547343500L;

	private Long idEntidad;
	private Long idBiblioteca;
	private String direccion;
	
	@Id
	@Column(name="ENTIDAD_ID")
	public Long getIdEntidad() {
		return idEntidad;
	}
	public void setIdEntidad(Long idEntidad) {
		this.idEntidad = idEntidad;
	}
	 
	@Column(name="BIBLIOTECA_ID")
	public Long getIdBiblioteca() {
		return idBiblioteca;
	}
	public void setIdBiblioteca(Long idBiblioteca) {
		this.idBiblioteca = idBiblioteca;
	}
	
	@Column
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	
}
