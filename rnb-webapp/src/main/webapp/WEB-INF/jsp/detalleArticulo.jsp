<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Articulos</title>
</head>
<body>
	<div class="detalle-articulo">
		<h3>Informacion de Articulo:</h3>
		<ul>
			<li>Codigo: ${articulo.codigo}</li>
			<li>Descripcion: ${articulo.descripcion}</li>
			<li>Stock: ${articulo.stock}</li>
			<li>Precio unitario: ${articulo.precio}</li>
		</ul>
	</div>
	<form method="POST" action="/minierp-web/articulos/eliminar/${articulo.codigo}">
		<input type="submit" name="Eliminar" value="Eliminar" /> 
		<input type="button" name="Regresar" value="<< Regresar" onclick="history.go(-1);" />
		<%-- 		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />  --%>
	</form>
</body>
</html>