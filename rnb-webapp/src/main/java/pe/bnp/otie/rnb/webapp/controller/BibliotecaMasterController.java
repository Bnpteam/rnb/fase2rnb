package pe.bnp.otie.rnb.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import pe.bnp.otie.rnb.proxy.BibliotecaMasterREST;
 
@Controller
@EnableWebMvc
@RequestMapping("/bibliotecaMaster")
public class BibliotecaMasterController {
	
	@Autowired
	BibliotecaMasterREST bibliotecaMasterRest;
	
	@RequestMapping(value = "/consultar", method = RequestMethod.GET)
	public String listarArticulos(Model model) {

		System.out.println("listando bibliotecas Master ");

		model.addAttribute("bibliotecasMaster", bibliotecaMasterRest.listar());

		return "listaBibliotecaMaster";
	}
}
